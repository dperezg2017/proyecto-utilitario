package com.utilitario.controller;

import com.utilitario.service.UtilitarioService;
import org.apache.commons.codec.digest.DigestUtils;
import org.bouncycastle.util.encoders.Hex;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

import javax.websocket.server.PathParam;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.MessageDigest;
import java.security.Security;
import java.util.Base64;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

@RestController
public class UtilitarioRest {

    @Autowired
    UtilitarioService utilitarioService;

    private static final Logger LOGGER = LoggerFactory.getLogger(UtilitarioRest.class);

    @GetMapping("/consumirExponerErrorConformidad")
    String consumirExponerErrorConformidad(){
        try {

            String nombreFile = "20112811096-DIS-20211202-01.zip";
            StringBuilder contenidoTxt = new StringBuilder();
            contenidoTxt.append("20112811096|01|F2333|23233233").append("\n");
            contenidoTxt.append("20112834543|02|F44444|343434").append("\n");

            File fileZip = new File(nombreFile.concat(".zip"));
            ZipOutputStream out = new ZipOutputStream(new FileOutputStream(fileZip));
            ZipEntry e = new ZipEntry(nombreFile.concat(".txt"));
            out.putNextEntry(e);

            byte[] data = contenidoTxt.toString().getBytes();
            out.write(data, 0, data.length);
            out.closeEntry();
            out.close();

            utilitarioService.obtenerErrorRest(fileZip, "6b3d9d6201de3508f15c7ec9ab610ec79daff6033b1566437d128d272693ec94");

            return "convertTXTtoZip";

        }catch (Exception e){
            e.printStackTrace();
            return "Error";
        }

    }

    @GetMapping("/consumirExponerArchivo")
    String getConvertTXTtoSHA(){

        try {

            String nombreFile = "20112811096-DIS-20211202-01.zip";
            StringBuilder contenidoTxt = new StringBuilder();
            contenidoTxt.append("20112811096|01|F2333|23233233").append("\n");
            contenidoTxt.append("20112834543|02|F44444|343434").append("\n");

            File fileZip = new File(nombreFile.concat(".zip"));
            ZipOutputStream out = new ZipOutputStream(new FileOutputStream(fileZip));
            ZipEntry e = new ZipEntry(nombreFile.concat(".txt"));
            out.putNextEntry(e);

            byte[] data = contenidoTxt.toString().getBytes();
            out.write(data, 0, data.length);
            out.closeEntry();
            out.close();

            utilitarioService.enviarRest(fileZip, "6b3d9d6201de3508f15c7ec9ab610ec79daff6033b1566437d128d272693ec94");

            return "convertTXTtoZip";

        }catch (Exception e){
            e.printStackTrace();
            return "Error";
        }

    }






    @GetMapping("/decryptSHA-256")
    String getdecryptSHA(@PathParam("codigo") String codigo){

        try{
            String result = DigestUtils.sha256Hex(codigo);
            return result;
        } catch(Exception ex){
            throw new RuntimeException(ex);
        }

    }


    private byte[] base64ToByte(String str) throws IOException {

        BASE64Decoder decoder = new BASE64Decoder();
        byte[] returnbyteArray = decoder.decodeBuffer(str);

        return returnbyteArray;
    }

    private String byteToBase64(byte[] bt) {

        BASE64Encoder endecoder = new BASE64Encoder();
        String returnString = endecoder.encode(bt);

        return returnString;
    }


    @GetMapping("/convertTXTtoZip")
    String getConvertTXTtoZip(){

        try {

            String nombreFile = "20112811096-01-20221022";
            StringBuilder contenidoTxt = new StringBuilder();
            contenidoTxt.append("20112811096|01|F2333|23233233").append("\n");
            contenidoTxt.append("20112834543|02|F44444|343434").append("\n");

            File fileZip = new File(nombreFile.concat(".zip"));
            ZipOutputStream out = new ZipOutputStream(new FileOutputStream(fileZip));
            ZipEntry e = new ZipEntry(nombreFile.concat(".txt"));
            out.putNextEntry(e);

            byte[] data = contenidoTxt.toString().getBytes();
            out.write(data, 0, data.length);
            out.closeEntry();
            out.close();

            // probando zip(txt), exportando el archivo
            byte[] bytes = Files.readAllBytes(fileZip.toPath());
            //byte[] bytesR = new byte[(int) fileZip.length()];
            Path path = Paths.get("D:\\probandoArchivo.zip");
            Files.write(path, bytes);

        }catch (Exception e){
            e.printStackTrace();
        }

        return "convertTXTtoZip";
    }


}
