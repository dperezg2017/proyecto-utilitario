package com.utilitario.domain;

import java.io.File;

public class EstadoRequest {
    private File archivo ;
    private String valHash;


    public File getArchivo() {
        return archivo;
    }

    public void setArchivo(File archivo) {
        this.archivo = archivo;
    }

    public String getValHash() {
        return valHash;
    }

    public void setValHash(String valHash) {
        this.valHash = valHash;
    }
}
