package com.utilitario.service;

import java.io.File;
import java.security.MessageDigest;

public interface UtilitarioService {

    String getFileChecksum(MessageDigest digest, File file);

    void enviarRest(File fileZip, String s);

    void obtenerErrorRest(File fileZip, String s);
}
