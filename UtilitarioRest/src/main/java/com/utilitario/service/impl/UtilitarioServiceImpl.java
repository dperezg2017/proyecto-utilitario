package com.utilitario.service.impl;

import com.utilitario.domain.EstadoRequest;
import com.utilitario.service.UtilitarioService;
import com.utilitario.util.Constantes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.io.File;
import java.io.FileInputStream;
import java.security.MessageDigest;

@Service
public class UtilitarioServiceImpl implements UtilitarioService {

    @Bean
    public RestTemplate restTemplate(){
        return new RestTemplate();
    }

    public void enviarRest(File fileZip, String valHash) {

        String url = "https://api-cpe.sunat.gob.pe/v1/contribuyente/controlcpe/movimientoscomprobante";
        HttpHeaders headers = new HttpHeaders();
//            headers.set(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
        headers.set("Content-Type", MediaType.APPLICATION_JSON_VALUE);
        headers.set("Authorization", Constantes.TOKEN);

        EstadoRequest estadoRequest = new EstadoRequest();
        estadoRequest.setArchivo(fileZip);
        estadoRequest.setValHash(valHash);

        HttpEntity<EstadoRequest> entity = new HttpEntity<>(estadoRequest, headers);
        ResponseEntity<String> response = restTemplate().exchange(url, HttpMethod.POST, entity, String.class);
        String responseSt= response.getBody();


    }

    @Override
    public void obtenerErrorRest(File fileZip, String valHash) {
        String url = "http://localhost:8090/getErrorConformidadEnviado";
        HttpHeaders headers = new HttpHeaders();
            headers.set(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
        headers.set("Content-Type", MediaType.APPLICATION_JSON_VALUE);
//        headers.set("Authorization", Constantes.TOKEN);

        EstadoRequest estadoRequest = new EstadoRequest();
        estadoRequest.setArchivo(fileZip);
        estadoRequest.setValHash(valHash);

        HttpEntity<EstadoRequest> entity = new HttpEntity<>(estadoRequest, headers);
        ResponseEntity<String> response = restTemplate().exchange(url, HttpMethod.POST, entity, String.class);
        String responseSt= response.getBody();

    }


    public String getFileChecksum(MessageDigest digest, File file) {

        try{

        //Get file input stream for reading the file content
        FileInputStream fis = new FileInputStream(file);

        //Create byte array to read data in chunks
        byte[] byteArray = new byte[1024];
        int bytesCount = 0;

        //Read file data and update in message digest
        while ((bytesCount = fis.read(byteArray)) != -1) {
            digest.update(byteArray, 0, bytesCount);
        };

        //close the stream; We don't need it now.
        fis.close();

        //Get the hash's bytes
        byte[] bytes = digest.digest();

        //This bytes[] has bytes in decimal format;
        //Convert it to hexadecimal format
        StringBuilder sb = new StringBuilder();
        for(int i=0; i< bytes.length ;i++)
        {
            sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
        }

        //return complete hash
        return sb.toString();

        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }


}
