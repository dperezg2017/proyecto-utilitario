package com.utilitario;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UtilitarioRestApplication {

	public static void main(String[] args) {
		SpringApplication.run(UtilitarioRestApplication.class, args);
	}

}
