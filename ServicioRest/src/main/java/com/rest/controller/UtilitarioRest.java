package com.rest.controller;
import com.rest.domain.Error;
import com.rest.domain.ErrorConformidadResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import java.util.ArrayList;
import java.util.List;

@RestController
public class UtilitarioRest {

    private static final Logger LOGGER = LoggerFactory.getLogger(UtilitarioRest.class);

    @PostMapping("/exponerArchivo")
    String getConvertTXTtoSHA(@RequestParam String valHash, @RequestParam MultipartFile archivo){

        String response=valHash+ " - "+ archivo.getName();
        LOGGER.info("response: {}",response);
            return valHash+ " - "+ archivo.getName();
    }

    @PostMapping("/getErrorConformidadEnviado")
    public ResponseEntity<?> getErrorConformidadEnviado(@RequestParam String valHash, @RequestParam MultipartFile archivo){

        try{
        ErrorConformidadResponse errorConformidadResponse = new ErrorConformidadResponse();
        errorConformidadResponse.setCod("422");
        errorConformidadResponse.setMsg("Unprocessable Entity - Se presentaron errores de validacion que impidieron completar el Request");
        errorConformidadResponse.setExc(null);
        List<Error> errors = new ArrayList<>();
        Error error = new Error();
        error.setCodError("210");
        error.setDesError("El archivo "+archivo.getOriginalFilename()+".txt fue previamente enviado");
        errors.add(error);
        errorConformidadResponse.setErrors(errors);

        return new ResponseEntity<ErrorConformidadResponse>(errorConformidadResponse,HttpStatus.UNPROCESSABLE_ENTITY);

        }catch (Exception e){
            LOGGER.error("Ocurrio un error: ",e);
            return new ResponseEntity<String>("errorConformidadResponse",HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }



}
