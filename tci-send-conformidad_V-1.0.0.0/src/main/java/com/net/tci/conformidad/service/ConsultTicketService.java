package com.net.tci.conformidad.service;

import com.net.tci.conformidad.mapeo.ConformidadTicket;

public interface ConsultTicketService {

    void procesarConsultTicket(ConformidadTicket conformidadTicket);
}
