package com.net.tci.conformidad.service;

import com.net.tci.conformidad.entity.SisCorrelativoConformidad;

public interface SisCorrelativoConformidadService {

    SisCorrelativoConformidad getSisCorrelativoConformidad(Integer idEmpresa);

    void updateSisCorrelativoConformidadByDis(Integer idEmpresa);

    void insertSisCorrelativoConformidad(Integer idEmpresaDistinct);

    void eliminarSisCorrelativoConformidad();
}
