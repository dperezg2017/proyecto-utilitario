package com.net.tci.conformidad.util;

import java.util.concurrent.Future;
import java.util.function.Predicate;

public class PredicateTaskDone implements Predicate<Future> {

    @Override
    public boolean test(Future future) {
        return future.isDone();
    }

}
