package com.net.tci.conformidad.service;

import com.net.tci.conformidad.entity.SisTicketConformidad;
import com.net.tci.conformidad.mapeo.ConformidadTicket;

import java.util.List;

public interface SisTicketConformidadService {

    Long saveSisTicketConformidad(SisTicketConformidad sisTicketConformidad);

    List<ConformidadTicket> getTicketConformidad(Integer cantidad);

    void updateEstadoDeshabilitado(Long idTicketConformidad);
}
