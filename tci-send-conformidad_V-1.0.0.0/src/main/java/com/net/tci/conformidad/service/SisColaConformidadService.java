package com.net.tci.conformidad.service;

import com.net.tci.conformidad.mapeo.ConformidadSisDatoTransaccion;

import java.util.List;

public interface SisColaConformidadService {

    List<ConformidadSisDatoTransaccion> getListaConformidadSisDatoTransaccion(Integer cantidad);

    void updateColaConformidadByIntento(StringBuilder listaTransacciones);

    void deleteColaConformidadByTransaccion(StringBuilder listaTransacciones);
}
