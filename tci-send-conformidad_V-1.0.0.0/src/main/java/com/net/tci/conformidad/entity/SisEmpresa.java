package com.net.tci.conformidad.entity;


import javax.persistence.*;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "sis_empresa")
public class SisEmpresa implements java.io.Serializable {

    private static final long serialVersionUID = -3490812332601791634L;

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "idEmpresa", unique = true, nullable = false)
    private Integer idEmpresa;

    @Column(name = "ruc", length = 15)
    private String ruc;

    @Column(name = "usuarioAcceso", length = 50)
    private String usuarioAcceso;

    @Column(name = "claveAcceso", length = 100)
    private String claveAcceso;

    @Column(name = "usuarioIdConformidad", length = 100)
    private String usuarioIdConformidad;

    @Column(name = "tokenGenerado", length = 100)
    private String tokenGenerado;

    @Column(name = "claveConformidad", length = 250)
    private String claveConformidad;

    @Column(name = "esConformidad", length = 1)
    private Boolean esConformidad;

    public Integer getIdEmpresa() {
        return idEmpresa;
    }

    public void setIdEmpresa(Integer idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    public String getUsuarioAcceso() {
        return usuarioAcceso;
    }

    public void setUsuarioAcceso(String usuarioAcceso) {
        this.usuarioAcceso = usuarioAcceso;
    }

    public String getClaveAcceso() {
        return claveAcceso;
    }

    public void setClaveAcceso(String claveAcceso) {
        this.claveAcceso = claveAcceso;
    }

    public String getUsuarioIdConformidad() {
        return usuarioIdConformidad;
    }

    public void setUsuarioIdConformidad(String usuarioIdConformidad) {
        this.usuarioIdConformidad = usuarioIdConformidad;
    }

    public String getRuc() {
        return ruc;
    }

    public void setRuc(String ruc) {
        this.ruc = ruc;
    }

    public String getTokenGenerado() {
        return tokenGenerado;
    }

    public void setTokenGenerado(String tokenGenerado) {
        this.tokenGenerado = tokenGenerado;
    }

    public String getClaveConformidad() {
        return claveConformidad;
    }

    public void setClaveConformidad(String claveConformidad) {
        this.claveConformidad = claveConformidad;
    }

    public Boolean getEsConformidad() {
        return esConformidad;
    }

    public void setEsConformidad(Boolean esConformidad) {
        this.esConformidad = esConformidad;
    }
}
