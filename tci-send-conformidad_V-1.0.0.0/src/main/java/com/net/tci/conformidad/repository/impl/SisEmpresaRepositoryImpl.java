package com.net.tci.conformidad.repository.impl;

import com.net.tci.conformidad.entity.SisEmpresa;
import com.net.tci.conformidad.repository.HibernateRepository;
import com.net.tci.conformidad.repository.SisEmpresaRepository;
import org.hibernate.Query;
import org.hibernate.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public class SisEmpresaRepositoryImpl extends HibernateRepository<SisEmpresa, Long> implements SisEmpresaRepository {

    private static final Logger LOGGER = LoggerFactory.getLogger(SisEmpresaRepositoryImpl.class);

    @Transactional(readOnly = true)
    public SisEmpresa getSisEmpresaByIdEmpresa(Integer idEmpresa) {
        try {
            String hql = "from SisEmpresa e where e.idEmpresa =:idEmpresa";
            Session session = sessionFactory.getCurrentSession();
            Query query = session.createQuery(hql);
            query.setParameter("idEmpresa", idEmpresa);
            return (SisEmpresa) query.uniqueResult();
        } catch (Exception e) {
            LOGGER.error("[getSisEmpresaByIdEmpresa] - Ocurrio un error: ", e);
            return null;
        }
    }

    @Transactional(readOnly = true)
    public SisEmpresa getSisEmpresaByRuc(String ruc) {
        try {
            String hql = "from SisEmpresa e where e.ruc =:ruc";
            Session session = sessionFactory.getCurrentSession();
            Query query = session.createQuery(hql);
            query.setParameter("ruc", ruc);
            return (SisEmpresa) query.uniqueResult();
        } catch (Exception e) {
            LOGGER.error("[getSisEmpresaByRuc] - Ocurrio un error: ", e);
            return null;
        }
    }
}
