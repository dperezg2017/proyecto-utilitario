package com.net.tci.conformidad.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadPoolExecutor;

public class ThreadUtil {
    private static final Logger LOGGER = LoggerFactory.getLogger(ThreadUtil.class);

    private ThreadUtil() {

    }

    public static Boolean waitForFinalizeThreads(ExecutorService executorService, List<Future> estadoHilos, int cantidadHilos) {
        ThreadPoolExecutor threadPoolExecutor = (ThreadPoolExecutor) executorService;
        Integer sleepTheard = 0;
        do {
            //   LOGGER.info("[waitForFinalizeThreads] - Inicio - Hilos activos {} de {} aprox; Tareas completadas {} de {} aprox", threadPoolExecutor.getLargestPoolSize(), cantidadHilos,
            //       threadPoolExecutor.getCompletedTaskCount(), threadPoolExecutor.getTaskCount());
            try {
                //     LOGGER.info("[waitForFinalizeThreads] - Hilos activos {}.", threadPoolExecutor.getActiveCount());
                if (sleepTheard >= UtilProperties.TIEMPO_PERMITIDO_CIERRE_HILOS) {
                    //     LOGGER.info("[waitForFinalizeThreads] - Se supero el tiempo permitido para ejecucion de hilos {} milisegundos. Forzando el cierre de todas las tareas pendientes {}.", UtilProperties.TIEMPO_PERMITIDO_CIERRE_HILOS, threadPoolExecutor.getActiveCount());
                    executorService.shutdownNow();
                    return Boolean.FALSE;
                } else {
                    sleepTheard += UtilProperties.TIEMPO_HILO;
                    // LOGGER.info("[waitForFinalizeThreads] - Se espera {} milisegundos, existen tareas pendientes, total esperado {}.", UtilProperties.TIEMPO_HILO, sleepTheard);
                    Thread.sleep(UtilProperties.TIEMPO_HILO);
                }
            } catch (Exception e) {
                LOGGER.error("[waitForFinalizeThreads] - Sucedio un error al dormir el hilo principal : {}", e.getMessage(), e);
            }
        } while (estadoHilos.stream().filter(new PredicateTaskDone()).count() != estadoHilos.size());

        // LOGGER.info("[waitForFinalizeThreads] - Fin - Hilos activos {} de {} aprox; Tareas completadas {} de {} aprox", threadPoolExecutor.getLargestPoolSize(), cantidadHilos,
        //        threadPoolExecutor.getCompletedTaskCount(), threadPoolExecutor.getTaskCount());
        return Boolean.TRUE;
    }

    public static Boolean waitForCloseThreads(ExecutorService executorService, int cantidadHilos) {
        ThreadPoolExecutor threadPoolExecutor = (ThreadPoolExecutor) executorService;
       // LOGGER.info("[waitForCloseThreads] - Finalizando Hilos:  activos {} de {} aprox; Tareas completadas {} de {} aprox", threadPoolExecutor.getActiveCount(), cantidadHilos,
         //       threadPoolExecutor.getCompletedTaskCount(), threadPoolExecutor.getTaskCount());
        try {
            Integer sleepTheard = 0;
            do {
            //    LOGGER.info("[waitForCloseThreads] - Hilos activos {}.", threadPoolExecutor.getActiveCount());
                if (threadPoolExecutor.getActiveCount() == 0) {
                    executorService.shutdown();
                    //         LOGGER.info("[waitForCloseThreads] - ShutDown ExecutorService... Task completed OK.");
                    return Boolean.TRUE;
                } else {
                    sleepTheard += UtilProperties.TIEMPO_HILO;
                    //         LOGGER.info("[waitForCloseThreads] - Se espera {} milisegundos, existen tareas pendientes, total esperado {}.", UtilProperties.TIEMPO_HILO, sleepTheard);
                    Thread.sleep(UtilProperties.TIEMPO_HILO);
                }
            } while (sleepTheard <= UtilProperties.TIEMPO_PERMITIDO_CIERRE_HILOS);
            //     LOGGER.info("[waitForCloseThreads] - Forzando el cierre de todas las tareas pendientes {}...", threadPoolExecutor.getActiveCount());
            executorService.shutdownNow();
            Thread.currentThread().interrupt();
            return Boolean.TRUE;
        } catch (InterruptedException e) {
            LOGGER.error("[waitForCloseThreads] - Error InterruptedException: ", e);
            executorService.shutdownNow();
            Thread.currentThread().interrupt();
        }
        return Boolean.FALSE;
    }


}
