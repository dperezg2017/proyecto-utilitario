package com.net.tci.conformidad.repository;

import com.net.tci.conformidad.entity.SisTicketConformidad;
import com.net.tci.conformidad.mapeo.ConformidadTicket;

import java.util.List;

public interface SisTicketConformidadRepository {

    Long saveSisTicketConformidad(SisTicketConformidad sisTicketConformidad);

    List<ConformidadTicket> getTicketConformidad(Integer cantidad);

    void updateEstadoDeshabilitado(Long idTicketConformidad);
}
