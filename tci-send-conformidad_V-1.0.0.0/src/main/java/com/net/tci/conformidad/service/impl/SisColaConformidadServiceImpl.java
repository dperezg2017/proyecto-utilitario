package com.net.tci.conformidad.service.impl;

import com.net.tci.conformidad.mapeo.ConformidadSisDatoTransaccion;
import com.net.tci.conformidad.repository.SisColaConformidadRepository;
import com.net.tci.conformidad.service.SisColaConformidadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SisColaConformidadServiceImpl implements SisColaConformidadService {


    @Autowired
    SisColaConformidadRepository sisColaConformidadRepository;

    public List<ConformidadSisDatoTransaccion> getListaConformidadSisDatoTransaccion(Integer cantidad) {
        return sisColaConformidadRepository.getListaConformidadSisDatoTransaccion(cantidad);
    }

    public void updateColaConformidadByIntento(StringBuilder listaTransacciones) {
        sisColaConformidadRepository.updateColaConformidadByIntento(listaTransacciones);
    }

    public void deleteColaConformidadByTransaccion(StringBuilder listaTransacciones) {
        sisColaConformidadRepository.deleteColaConformidadByTransaccion(listaTransacciones);
    }

}
