package com.net.tci.conformidad.service;

import com.net.tci.conformidad.mapeo.ConformidadSisDatoTransaccion;

import java.util.List;

public interface SendStateService {

    void procesarSendState(Integer idEmpresaDistinct, List<ConformidadSisDatoTransaccion> conformidadSisDatoTransaccionList);
}
