package com.net.tci.conformidad.mapeo;

import java.util.Date;

public class ConformidadTicket implements java.io.Serializable {

    private static final long serialVersionUID = -3490813442601791634L;

    Long idTicketConformidad;
    String ticket;
    String nombreArchivo;
    Date fechaRegistroTicket;
    Boolean estado;

    public Boolean getEstado() {
        return estado;
    }

    public void setEstado(Boolean estado) {
        this.estado = estado;
    }

    public Long getIdTicketConformidad() {
        return idTicketConformidad;
    }

    public void setIdTicketConformidad(Long idTicketConformidad) {
        this.idTicketConformidad = idTicketConformidad;
    }

    public String getTicket() {
        return ticket;
    }

    public void setTicket(String ticket) {
        this.ticket = ticket;
    }

    public String getNombreArchivo() {
        return nombreArchivo;
    }

    public void setNombreArchivo(String nombreArchivo) {
        this.nombreArchivo = nombreArchivo;
    }

    public Date getFechaRegistroTicket() {
        return fechaRegistroTicket;
    }

    public void setFechaRegistroTicket(Date fechaRegistroTicket) {
        this.fechaRegistroTicket = fechaRegistroTicket;
    }
}
