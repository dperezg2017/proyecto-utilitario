package com.net.tci.conformidad.service.impl;

import com.net.tci.conformidad.entity.SisCorrelativoConformidad;
import com.net.tci.conformidad.repository.SisCorrelativoConformidadRepository;
import com.net.tci.conformidad.service.SisCorrelativoConformidadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SisCorrelativoConformidadServiceImpl implements SisCorrelativoConformidadService {

    @Autowired
    SisCorrelativoConformidadRepository sisCorrelativoConformidadRepository;

    public SisCorrelativoConformidad getSisCorrelativoConformidad(Integer idEmpresa) {
        return sisCorrelativoConformidadRepository.getSisCorrelativoConformidad(idEmpresa);
    }

    public void updateSisCorrelativoConformidadByDis(Integer idEmpresa) {
        sisCorrelativoConformidadRepository.updateSisCorrelativoConformidadByDis(idEmpresa);
    }

    public void insertSisCorrelativoConformidad(Integer idEmpresa) {
        sisCorrelativoConformidadRepository.insertSisCorrelativoConformidad(idEmpresa);
    }

    public void eliminarSisCorrelativoConformidad() {
        sisCorrelativoConformidadRepository.eliminarSisCorrelativoConformidad();
    }
}
