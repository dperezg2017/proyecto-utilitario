package com.net.tci.conformidad.repository.impl;

import com.net.tci.conformidad.entity.SisDatoTransaccionConformidad;
import com.net.tci.conformidad.repository.HibernateRepository;
import com.net.tci.conformidad.repository.SisDatoTransaccionConformidadRepository;
import org.hibernate.Query;
import org.hibernate.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public class SisDatoTransaccionConformidadRepositoryImpl extends HibernateRepository<SisDatoTransaccionConformidad, Long>
        implements SisDatoTransaccionConformidadRepository {

    private static final Logger LOGGER = LoggerFactory.getLogger(SisDatoTransaccionConformidadRepositoryImpl.class);

    @Transactional
    public void updateIdEstadoConformidadSunatAndIdTicketConformidad(Long idTicketConformidad, StringBuilder contTransaccionList, Integer tipo) {
        try {
            String hql = "UPDATE sis_dato_transaccion_conformidad SET idEstadoConformidadSunat=:tipo, idTicketConformidad=:idTicketConformidad WHERE idTransaccion IN (" + contTransaccionList + ")";
            Session session = sessionFactory.getCurrentSession();
            Query query = session.createSQLQuery(hql);
            query.setParameter("tipo", tipo);
            query.setParameter("idTicketConformidad", idTicketConformidad);
            query.executeUpdate();
        } catch (Exception e) {
            LOGGER.error("[updateColaConformidadByIntento] - Ocurrio un error: ", e);
        }
    }

    @Transactional
    public void updateFilaTicketError(String sqlUpdateError) {
        try {
            Session session = sessionFactory.getCurrentSession();
            Query query = session.createSQLQuery(sqlUpdateError);
            query.executeUpdate();
        } catch (Exception e) {
            LOGGER.error("[updateFilaTicketError] - Ocurrio un error: ", e);
        }
    }

    @Transactional
    public void updateIdEstadoConformidadConformeDisconforme(Long idTicketConformidad) {
        try {
            String hql = "UPDATE sis_dato_transaccion_conformidad " +
                    " SET idEstadoConformidad = IF(idEstadoConformidadSunat=1,8,2) " +
                    " WHERE idTicketConformidad=:idTicketConformidad";
            Session session = sessionFactory.getCurrentSession();
            Query query = session.createSQLQuery(hql);
            query.setParameter("idTicketConformidad", idTicketConformidad);
            query.executeUpdate();
        } catch (Exception e) {
            LOGGER.error("[updateIdEstadoConformidadConformeDisconforme] - Ocurrio un error: ", e);
        }
    }
}
