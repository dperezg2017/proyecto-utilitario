package com.net.tci.conformidad.service.impl;

import com.google.gson.Gson;
import com.net.tci.conformidad.domain.rest.ConsultarTicketResponse;
import com.net.tci.conformidad.entity.SisEmpresa;
import com.net.tci.conformidad.mapeo.ConformidadTicket;
import com.net.tci.conformidad.service.ConsultTicketService;
import com.net.tci.conformidad.service.SisDatoTransaccionConformidadService;
import com.net.tci.conformidad.service.SisEmpresaService;
import com.net.tci.conformidad.service.SisTicketConformidadService;
import com.net.tci.conformidad.util.Constantes;
import com.net.tci.conformidad.util.UtilProperties;
import com.net.tci.conformidad.util.Utilitario;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.Base64;
import java.util.stream.Stream;

@Component
public class ConsultTicketServiceImpl implements ConsultTicketService {

    private static final Logger LOGGER = LoggerFactory.getLogger(ConsultTicketServiceImpl.class);

    @Autowired
    SisEmpresaService sisEmpresaService;

    @Autowired
    SisDatoTransaccionConformidadService sisDatoTransaccionConformidadService;

    @Autowired
    SisTicketConformidadService sisTicketConformidadService;

    public void procesarConsultTicket(ConformidadTicket conformidadTicket) {

        CloseableHttpResponse response = consumerGetRest(conformidadTicket.getTicket(), conformidadTicket.getNombreArchivo());
        evaluarRespuesta(response, conformidadTicket.getIdTicketConformidad());

    }

    private void evaluarRespuesta(CloseableHttpResponse response, Long idTicketConformidad) {
        String json;
        ConsultarTicketResponse consultarTicketResponse;
        try {
            HttpEntity responseEntity = response.getEntity();
            if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                json = IOUtils.toString(responseEntity.getContent(), StandardCharsets.UTF_8);
                consultarTicketResponse = new Gson().fromJson(json, ConsultarTicketResponse.class);
                if (consultarTicketResponse.getCodEstadoProceso().equalsIgnoreCase(Constantes.PROCESADO_ERRORES)) {
                    LOGGER.info("[evaluarRespuesta] - Se obtuvo estado PROCESADO CON ERRORES del ticket: {} se procede a deshabilitarlo", idTicketConformidad);
                    procesaError(consultarTicketResponse,idTicketConformidad);
                } else if (consultarTicketResponse.getCodEstadoProceso().equalsIgnoreCase(Constantes.PROCESADO)) {
                    sisTicketConformidadService.updateEstadoDeshabilitado(idTicketConformidad);
                    sisDatoTransaccionConformidadService.updateIdEstadoConformidadConformeDisconforme(idTicketConformidad);
                    LOGGER.info("[evaluarRespuesta] - Se obtuvo estado PROCESADO del ticket: {} se procede a deshabilitarlo", idTicketConformidad);
                } else if (consultarTicketResponse.getCodEstadoProceso().equalsIgnoreCase(Constantes.CARGADO)) {
                    LOGGER.info("[evaluarRespuesta] - Se obtuvo estado CARGADO del ticket: {} se procede a reintentar", idTicketConformidad);
                } else if (consultarTicketResponse.getCodEstadoProceso().equalsIgnoreCase(Constantes.VALIDANDO_ARCHIVO)) {
                    LOGGER.info("[evaluarRespuesta] - Se obtuvo estado VALIDANDO ARCHIVO del ticket: {} se procede a reintentar", idTicketConformidad);
                }
            }


            response.close();
        } catch (Exception e) {
            LOGGER.error("[evaluarRespuesta] - Ocurrio un error: ", e);
            try {
                response.close();
            } catch (IOException ex) {
                LOGGER.error("[evaluarRespuesta] - Ocurrio un error: ", ex);
            }
        }
    }

    private void procesaError(ConsultarTicketResponse consultarTicketResponse, Long idTicketConformidad) {
        byte[] decoded = Base64.getDecoder().decode(consultarTicketResponse.getArcErrores());
        File decodedFile = new File(Constantes.ARCHIVO_TXT_ARCHIVO);
        try {
            FileUtils.writeByteArrayToFile(decodedFile, decoded);
            Stream<String> stream = Files.lines(decodedFile.toPath());
            stream.forEach(arcErrores -> {
                procesarFilaError(arcErrores, consultarTicketResponse.getDesArchivo(), idTicketConformidad);
            });
        } catch (IOException e) {
            LOGGER.error("[evaluarRespuesta] - Ocurrio un error: ", e);
        }finally {
            decodedFile.delete();
        }
    }

    private void procesarFilaError(String arcErrores, String nombreArchivoTxt, Long idTicketConformidad) {
        String sqlUpdateError = getSqlToUpdateError(idTicketConformidad, arcErrores, nombreArchivoTxt);
        sisDatoTransaccionConformidadService.updateFilaTicketError(sqlUpdateError);
        sisTicketConformidadService.updateEstadoDeshabilitado(idTicketConformidad);


    }

    private String getSqlToUpdateError(Long idTicketConformidad, String arcErrores, String nombreArchivoTxt) {
        String[] arcError = arcErrores.split(Constantes.SPLIT_BARRA);
        String rucEmisor = arcError[0];
        Integer idTipoDocumento = (arcError[1].equalsIgnoreCase(Constantes.COD_FACTURA) ? Constantes.FACTURA : Constantes.RH);
        String serieNumero = arcError[2].concat(Constantes.GUION).concat(arcError[3]);
        String descErrorTicket = arcError[arcError.length - Constantes.N_UNO];
        String[] nombreArchivo = StringUtils.split(nombreArchivoTxt, Constantes.GUION);
        String rucReceptor = nombreArchivo[0];

        LOGGER.info("[getSqlToUpdateError] - se actualizara estado error del ticket: {} - rucEmisor: {} - idTipoDocumento: {} - serieNumero: {} - rucReceptor: {}",
                idTicketConformidad, rucEmisor, idTipoDocumento, serieNumero, rucReceptor);

        return "UPDATE sis_dato_transaccion_conformidad SET idEstadoConformidad=9, " +
                " descripcionErrorTicket= '" + descErrorTicket + "'" +
                " WHERE idTransaccion=(" +
                " SELECT idTransaccion FROM sis_dato_transaccion " +
                " WHERE ruc_emisor='" + rucEmisor + "'" +
                " AND serie_documento='" + serieNumero + "'" +
                " AND idTipoDocumento=" + idTipoDocumento +
                " AND ruc_receptor='" + rucReceptor + "'" +
                " )";
    }

    private CloseableHttpResponse consumerGetRest(String ticket, String nombreArchivo) {

        SisEmpresa sisEmpresa = sisEmpresaService.getSisEmpresaByRuc(Utilitario.obtenerRuc(nombreArchivo));

        CloseableHttpClient client = HttpClientBuilder.create().build();
        HttpGet httpGet = new HttpGet(UtilProperties.URL_CONSULTAR_TICKET);
        try {
            URI uri = new URIBuilder(httpGet.getURI())
                    .addParameter(Constantes.CAMPO_NUMTICKET, ticket)
                    .build();
            ((HttpRequestBase) httpGet).setURI(uri);
            // descomentar cuando pase a produccion
            //httpGet.setHeader("Authorization", sisEmpresa.getTokenGenerado());
            CloseableHttpResponse response = client.execute(httpGet);
            return response;
        } catch (Exception e) {
            LOGGER.error("[consumerGetRest] - Ocurrio un error: ", e);
            return null;
        }
    }
}
