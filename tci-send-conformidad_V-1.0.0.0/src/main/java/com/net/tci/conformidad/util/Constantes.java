package com.net.tci.conformidad.util;

public class Constantes {

    private Constantes() {
    }

    public static final Integer COD_CONFORMIDAD = 1;
    public static final Integer COD_DISCONFORMIDAD = 2;
    public static final Integer COD_CONFORMIDAD_DISCONFORMIDAD = 3;
    public static final String CAMPO_VALHASH = "valHash";
    public static final String CAMPO_ARCHIVO = "archivo";
    public static final String CAMPO_NUMTICKET = "numTicket";
    public static final String ARCHIVO_TXT_ARCHIVO = "ARCHIVO_TXT_ERROR";
    public static final String PROCESADO_ERRORES = "03";
    public static final String PROCESADO = "04";
    public static final String CARGADO = "02";
    public static final String VALIDANDO_ARCHIVO = "01";
    public static final String T_CERO = "0";
    public static final String T_UNO = "1";
    public static final Integer N_CERO = 0;
    public static final Integer N_UNO = 1;
    public static final Integer N_DIEZ = 10;
    public static final Integer N_CIEN = 100;
    public static final String FORMAT_DATE_YYYYMMDD = "yyyyMMdd";
    public static final String COD_FACTURA = "01";
    public static final Integer FACTURA = 1;
    public static final String COD_RH = "02";
    public static final Integer RH = 2;
    public static final Integer CORRELATIVO_NUEVO = 1;
    public static final Integer COD_SERIE = 0;
    public static final Integer COD_NUMERO = 1;
    public static final String GUION = "-";
    public static final String PND = "PND";
    public static final String PUNTO = ".";
    public static final String TXT = "txt";
    public static final String ZIP = "zip";
    public static final String BARRA = "|";
    public static final String SPLIT_BARRA = "\\|";
    public static final String SHA_256 = "SHA-256";
    public static final String TOKEN = "eyJraWQiOiJhcGkuc3VuYXQuZ29iLnBlLmtpZDAwMSIsInR5cCI6IkpXVCIsImFsZyI6IlJTMjU2In0.eyJzdWIiOiIyMDExMjgxMTA5NiIsImF1ZCI6Ilt7XCJhcGlcIjpcImh0dHBzOlwvXC9hcGktY3BlLnN1bmF0LmdvYi5wZVwiLFwicmVjdXJzb1wiOlt7XCJpZFwiOlwiXC92MVwvY29udHJpYnV5ZW50ZVwvY29udHJvbGNwZVwiLFwiaW5kaWNhZG9yXCI6XCIxXCIsXCJndFwiOlwiMTAwMDAwXCJ9XX1dIiwidXNlcmRhdGEiOnsibnVtUlVDIjoiMjAxMTI4MTEwOTYiLCJ0aWNrZXQiOiIxODExNTA1NTk1MjEiLCJucm9SZWdpc3RybyI6IiIsImFwZU1hdGVybm8iOiIiLCJsb2dpbiI6IjIwMTEyODExMDk2Sk9ITjQzNTMiLCJub21icmVDb21wbGV0byI6IlQuQy5JLlMuQS5UUkFOU1BPUlRFIENPTkZJREVOQ0lBTCBERSBJTkYgT1JNQUNJT04iLCJub21icmVzIjoiVC5DLkkuUy5BLlRSQU5TUE9SVEUgQ09ORklERU5DSUFMIERFIElORiBPUk1BQ0lPTiIsImNvZERlcGVuZCI6IjAwMjEiLCJjb2RUT3BlQ29tZXIiOiIiLCJjb2RDYXRlIjoiIiwibml2ZWxVTyI6MCwiY29kVU8iOiIiLCJjb3JyZW8iOiIiLCJ1c3VhcmlvU09MIjoiSk9ITjQzNTMiLCJpZCI6IiIsImRlc1VPIjoiIiwiZGVzQ2F0ZSI6IiIsImFwZVBhdGVybm8iOiIiLCJpZENlbHVsYXIiOm51bGwsIm1hcCI6eyJpc0Nsb24iOmZhbHNlLCJkZHBEYXRhIjp7ImRkcF9udW1ydWMiOiIyMDExMjgxMTA5NiIsImRkcF9udW1yZWciOiIwMDIxIiwiZGRwX2VzdGFkbyI6IjAwIiwiZGRwX2ZsYWcyMiI6IjAwIiwiZGRwX3ViaWdlbyI6IjE1MDEzMSIsImRkcF90YW1hbm8iOiIwMSIsImRkcF90cG9lbXAiOiIyNiIsImRkcF9jaWl1IjoiNzI5MDkifSwiaWRNZW51IjoiMTgxMTUwNTU5NTIxIiwiam5kaVBvb2wiOiJwMDAyMSIsInRpcFVzdWFyaW8iOiIxIiwidGlwT3JpZ2VuIjoiSVQiLCJwcmltZXJBY2Nlc28iOnRydWV9fSwibmJmIjoxNjQ3OTc5NTU5LCJjbGllbnRJZCI6ImY5MWM4NTU5LTliYWEtNDQ0Yy1iZjI2LTA2ZDRjZGJmNzQ5NiIsImlzcyI6Imh0dHBzOlwvXC9hcGktc2VndXJpZGFkLnN1bmF0LmdvYi5wZVwvdjFcL2NsaWVudGVzc29sXC9mOTFjODU1OS05YmFhLTQ0NGMtYmYyNi0wNmQ0Y2RiZjc0OTZcL29hdXRoMlwvdG9rZW5cLyIsImV4cCI6MTY0Nzk4MzE1OSwiZ3JhbnRUeXBlIjoicGFzc3dvcmQiLCJpYXQiOjE2NDc5Nzk1NTl9.CJ4CgqcqmJ2vy6coT4_Au7orPSbVWqo_-EEnqdM6M-c9mJPN4qk9zp0Fi5weuhglXuTx-A0VE4X7JorgY8AmTuRCvrkVIBz4aY7kr0uLMr76NP5s3LT7VIb8PXARJgxHoGXGo0VAJfncoMA_HKh4WdXi8fZLTPPuPheotHPcH0-Ar_x7LNZ5kH6P0rwr1Cxx2LnOB-r_NS7Zt6trHcOiW1vKJlX8FYlZcBRBl8TnOxiEeO_PacPU808QsfIIyU3b-2Wm9vz2hcQipK9ppehoDaSF-T2IMacJJ6-vNSVYDl_JHp_wuRFarn8U6NhKEKy2RHEa39SiTirlmHN4W9v_lg";

}
