package com.net.tci.conformidad.service.impl;

import com.net.tci.conformidad.entity.SisEmpresa;
import com.net.tci.conformidad.repository.SisEmpresaRepository;
import com.net.tci.conformidad.service.SisEmpresaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SisEmpresaServiceImpl implements SisEmpresaService {

    @Autowired
    SisEmpresaRepository sisEmpresaRepository;

    public SisEmpresa getSisEmpresaByIdEmpresa(Integer idEmpresa) {
        return sisEmpresaRepository.getSisEmpresaByIdEmpresa(idEmpresa);
    }

    @Override
    public SisEmpresa getSisEmpresaByRuc(String ruc) {
        return sisEmpresaRepository.getSisEmpresaByRuc(ruc);
    }

}
