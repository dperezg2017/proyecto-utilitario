package com.net.tci.conformidad.domain.rest;

public class EstadoResponse {

    private String numTicket;

    public String getNumTicket() {
        return numTicket;
    }

    public void setNumTicket(String numTicket) {
        this.numTicket = numTicket;
    }
}
