package com.net.tci.conformidad.repository;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

public interface GenericRepository<T, I extends Serializable> {

    void add(T t);

    void addCommit(T t);

    boolean addFlush(T t);

    boolean addFlushString(T t);

    void update(T t);

    void delete(T t);

    T findById(I id);

    List<T> findAll();

    void saveOrUpdate(T t);

    void addCollection(Collection<T> lstEntidad);

}
