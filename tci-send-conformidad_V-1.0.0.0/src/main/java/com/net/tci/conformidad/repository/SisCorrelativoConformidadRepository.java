package com.net.tci.conformidad.repository;

import com.net.tci.conformidad.entity.SisCorrelativoConformidad;

public interface SisCorrelativoConformidadRepository {

    SisCorrelativoConformidad getSisCorrelativoConformidad(Integer idEmpresa);

    void updateSisCorrelativoConformidadByDis(Integer idEmpresa);

    void insertSisCorrelativoConformidad(Integer idEmpresa);

    void eliminarSisCorrelativoConformidad();
}
