package com.net.tci.conformidad.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;

import java.io.*;
import java.security.MessageDigest;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class Utilitario {

    private static final Logger LOGGER = LoggerFactory.getLogger(Utilitario.class);

    public static String obtenerNumeroSerie(String cadena, Integer tipo) {
        String[] documento = StringUtils.split(cadena, Constantes.GUION);
        if (tipo.equals(0))
            return documento[0];
        else
            return documento[1];
    }

    public static String obtenerRuc(String nombreArchivo) {
        String[] documento = StringUtils.split(nombreArchivo, Constantes.GUION);
        return documento[0];
    }

    public static String generarCodigoFileTxt(String nombreFile, StringBuilder contenidoFileTxt, String algoritmo) {

        try {
            MessageDigest messageDigest = MessageDigest.getInstance(algoritmo);
            File fileTxt = new File(nombreFile.concat(Constantes.PUNTO).concat(Constantes.TXT));
            FileWriter fileWriter = new FileWriter(fileTxt);
            BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
            PrintWriter printWriter = new PrintWriter(bufferedWriter);
            printWriter.write(contenidoFileTxt.toString());
            printWriter.close();
            bufferedWriter.close();
            String fileChecksum = generarFileChecksum(messageDigest, fileTxt);
            fileTxt.delete();
            return fileChecksum;

        } catch (Exception e) {
            LOGGER.error("[generarCodigoFileTxt] - Ocurrio un error: ", e);
            return null;
        }
    }

    public static String generarFileChecksum(MessageDigest digest, File file) {

        try {

            FileInputStream fis = new FileInputStream(file);
            byte[] byteArray = new byte[1024];
            int bytesCount = 0;

            while ((bytesCount = fis.read(byteArray)) != -1) {
                digest.update(byteArray, 0, bytesCount);
            }
            ;

            fis.close();

            byte[] bytes = digest.digest();
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < bytes.length; i++) {
                sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
            }

            return sb.toString();

        } catch (Exception e) {
            LOGGER.error("[generarFileChecksum] - Ocurrio un error: ", e);
            return null;
        }
    }

    public static File generarTxtZip(String nombreFile, StringBuilder contenidoFileTxt) {
        try {

            StringBuilder contenidoTxt = new StringBuilder();
            contenidoTxt.append(contenidoFileTxt);
            File fileZip = new File(nombreFile.concat(Constantes.PUNTO).concat(Constantes.ZIP));
            ZipOutputStream out = new ZipOutputStream(new FileOutputStream(fileZip));
            ZipEntry e = new ZipEntry(nombreFile.concat(Constantes.PUNTO).concat(Constantes.TXT));
            out.putNextEntry(e);

            byte[] data = contenidoTxt.toString().getBytes();
            out.write(data, 0, data.length);
            out.closeEntry();
            out.close();

            return fileZip;
        } catch (Exception e) {
            LOGGER.error("[generarTxtZip] - Ocurrio un error: ", e);
            return null;
        }
    }

}
