package com.net.tci.conformidad.task.async;

import com.net.tci.conformidad.service.SisCorrelativoConformidadService;
import com.net.tci.conformidad.task.Task;
import com.net.tci.conformidad.util.DispatcherThreadFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Component
@Profile("asyncEliminaCorrelativo")
public class AsyncEliminaCorrelativoTask implements Task {

    private static final Logger LOGGER = LoggerFactory.getLogger(AsyncEliminaCorrelativoTask.class);

    @Autowired
    SisCorrelativoConformidadService sisCorrelativoConformidadService;

    @Override
    public void executeTask() {
        long timeStart = System.currentTimeMillis();
        LOGGER.info("[asyncEliminaCorrelativo] - Inicio de proceso de AsyncEliminaCorrelativoTask: {}", new Date());
        ExecutorService executor = Executors.newSingleThreadExecutor(
                new DispatcherThreadFactory("EliminaCorrelativoDispatcher"));
        executor.submit(() -> sisCorrelativoConformidadService.eliminarSisCorrelativoConformidad());
        executor.shutdown();
        LOGGER.info("[asyncEliminaCorrelativo] - Tiempo utilizado de proceso: {} milliseconds.", (System.currentTimeMillis() - timeStart));
    }
}
