package com.net.tci.conformidad.repository;

import com.net.tci.conformidad.mapeo.ConformidadSisDatoTransaccion;

import java.util.List;

public interface SisColaConformidadRepository {

    List<ConformidadSisDatoTransaccion> getListaConformidadSisDatoTransaccion(Integer cantidad);

    void updateColaConformidadByIntento(StringBuilder listaTransacciones);

    void deleteColaConformidadByTransaccion(StringBuilder listaTransacciones);

}
