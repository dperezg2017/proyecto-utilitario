package com.net.tci.conformidad.service.impl;

import com.google.gson.Gson;
import com.net.tci.conformidad.domain.rest.EstadoResponse;
import com.net.tci.conformidad.entity.SisCorrelativoConformidad;
import com.net.tci.conformidad.entity.SisEmpresa;
import com.net.tci.conformidad.entity.SisTicketConformidad;
import com.net.tci.conformidad.mapeo.ConformidadSisDatoTransaccion;
import com.net.tci.conformidad.service.*;
import com.net.tci.conformidad.util.Constantes;
import com.net.tci.conformidad.util.UtilProperties;
import com.net.tci.conformidad.util.Utilitario;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.File;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Component
public class SendStateServiceImpl implements SendStateService {

    private static final Logger LOGGER = LoggerFactory.getLogger(SendStateServiceImpl.class);

    @Autowired
    SisEmpresaService sisEmpresaService;

    @Autowired
    SisCorrelativoConformidadService sisCorrelativoConformidadService;

    @Autowired
    SisColaConformidadService sisColaConformidadService;

    @Autowired
    SisTicketConformidadService sisTicketConformidadService;

    @Autowired
    SisDatoTransaccionConformidadService sisDatoTransaccionConformidadService;


    public void procesarSendState(Integer idEmpresaDistinct, List<ConformidadSisDatoTransaccion> conformidadSisDatoTransaccionList) {

        String nombreFile = null;
        StringBuilder contTransaccionList = getTransaccionList(Constantes.COD_CONFORMIDAD_DISCONFORMIDAD, idEmpresaDistinct, conformidadSisDatoTransaccionList);
        StringBuilder contTransaccionListConformidad = getTransaccionList(Constantes.COD_CONFORMIDAD, idEmpresaDistinct, conformidadSisDatoTransaccionList);
        StringBuilder contTransaccionListDisconformidad = getTransaccionList(Constantes.COD_DISCONFORMIDAD, idEmpresaDistinct, conformidadSisDatoTransaccionList);

        try {
            SisEmpresa sisEmpresaDistinct = sisEmpresaService.getSisEmpresaByIdEmpresa(idEmpresaDistinct);
            SisCorrelativoConformidad sisCorrelativoConformidad = sisCorrelativoConformidadService.getSisCorrelativoConformidad(idEmpresaDistinct);

            Integer correlativo = obtenerCorrelativo(idEmpresaDistinct, sisCorrelativoConformidad);

            if (correlativo == Constantes.N_CIEN) {
                LOGGER.info("[procesarSendState] - No se realiza ninguna accion para el idEmpresa: {} , ya que tiene 99 archivos enviado a sunat", idEmpresaDistinct);
            } else {

                nombreFile = getNombreFile(sisEmpresaDistinct.getRuc(), Constantes.PND, correlativo);
                StringBuilder contenidoFileTxt = getContenidoFileTxt(nombreFile, idEmpresaDistinct, conformidadSisDatoTransaccionList);
                File fileZip = Utilitario.generarTxtZip(nombreFile, contenidoFileTxt);
                String codigoShaChecksum = Utilitario.generarCodigoFileTxt(nombreFile, contenidoFileTxt, Constantes.SHA_256);
                HttpResponse response = consumerPostRest(fileZip, codigoShaChecksum);
                evaluarRespuesta(response, nombreFile, contTransaccionList, contTransaccionListConformidad, contTransaccionListDisconformidad);
                fileZip.delete();

                sisCorrelativoConformidadService.updateSisCorrelativoConformidadByDis(idEmpresaDistinct);
            }

        } catch (Exception e) {
            LOGGER.error("[procesarSendState] - Ocurrio un error con el archivo {} - Error: ", nombreFile, e.getMessage());
            sisColaConformidadService.updateColaConformidadByIntento(contTransaccionList);
        }

    }

    private void evaluarRespuesta(HttpResponse response, String nombreFile, StringBuilder contTransaccionList,
                                  StringBuilder contTransaccionListConformidad, StringBuilder contTransaccionListDisconformidad) {
        EstadoResponse estadoResponse;
        String json;
        try {
            HttpEntity responseEntity = response.getEntity();
            if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                json = IOUtils.toString(responseEntity.getContent(), StandardCharsets.UTF_8);
                estadoResponse = new Gson().fromJson(json, EstadoResponse.class);
                LOGGER.info("[evaluarRespuesta] - OK del archivo: {} -  ticket: {}", nombreFile, estadoResponse.getNumTicket());
                sisColaConformidadService.deleteColaConformidadByTransaccion(contTransaccionList);

                SisTicketConformidad sisTicketConformidad = mapearSisTicketConformidad(estadoResponse.getNumTicket(), nombreFile);
                Long idTicketConformidad = sisTicketConformidadService.saveSisTicketConformidad(sisTicketConformidad);

                if (contTransaccionListConformidad != null) {
                    sisDatoTransaccionConformidadService.updateIdEstadoConformidadSunatAndIdTicketConformidad(
                            idTicketConformidad, contTransaccionListConformidad, Constantes.COD_CONFORMIDAD);
                }
                if (contTransaccionListDisconformidad != null) {
                    sisDatoTransaccionConformidadService.updateIdEstadoConformidadSunatAndIdTicketConformidad(
                            idTicketConformidad, contTransaccionListDisconformidad, Constantes.COD_DISCONFORMIDAD);
                }

            } else {
                json = IOUtils.toString(responseEntity.getContent(), StandardCharsets.UTF_8);
                LOGGER.error("[evaluarRespuesta] - ERROR del archivo: {} - se actualiza reintento a las transacciones: {} - trama: {}",
                        nombreFile, contTransaccionList, json);
                sisColaConformidadService.updateColaConformidadByIntento(contTransaccionList);
            }
        } catch (Exception e) {
            LOGGER.error("[evaluarRespuesta] - Ocurrio un error: ", e);
        }
    }

    private SisTicketConformidad mapearSisTicketConformidad(String numTicket, String nombreFile) {

        SisTicketConformidad sisTicketConformidad = new SisTicketConformidad();
        sisTicketConformidad.setTicket(numTicket);
        sisTicketConformidad.setNombreArchivo(nombreFile);
        sisTicketConformidad.setFechaRegistroTicket(new Date());
        sisTicketConformidad.setEstado(true);
        return sisTicketConformidad;
    }

    private Integer obtenerCorrelativo(Integer idEmpresaDistinct, SisCorrelativoConformidad sisCorrelativoConformidad) {
        Integer correlativo;
        if (sisCorrelativoConformidad == null) {
            sisCorrelativoConformidadService.insertSisCorrelativoConformidad(idEmpresaDistinct);
            correlativo = Constantes.CORRELATIVO_NUEVO;
        } else {
            correlativo = sisCorrelativoConformidad.getCorrelativoDis() + Constantes.N_UNO;
        }
        return correlativo;
    }


    private HttpResponse consumerPostRest(File fileZip, String codigoShaChecksum) {
        CloseableHttpClient client = HttpClientBuilder.create().build();
        HttpPost httpPost = new HttpPost(UtilProperties.URL_OBTENER_TICKET);
        try {
            StringBody userBody = new StringBody(codigoShaChecksum, ContentType.MULTIPART_FORM_DATA);
            FileBody fileBody = new FileBody(fileZip, ContentType.DEFAULT_BINARY);

            MultipartEntityBuilder entityBuilder = MultipartEntityBuilder.create();
            entityBuilder.addPart(Constantes.CAMPO_VALHASH, userBody);
            entityBuilder.addPart(Constantes.CAMPO_ARCHIVO, fileBody);
            HttpEntity entity = entityBuilder.build();
            httpPost.setEntity(entity);
            // descomentar cuando pase a produccion
//            httpPost.setHeader("Authorization", sisEmpresaDistinct.getTokenGenerado());
            CloseableHttpResponse response = client.execute(httpPost);
            return response;
        } catch (Exception e) {
            LOGGER.error("[consumerPostRest] - Ocurrio un error: ", e);
            return null;
        }

    }

    private StringBuilder getContenidoFileTxt(String nombreFile, Integer idEmpresaDistinct, List<ConformidadSisDatoTransaccion> conformidadSisDatoTransaccionList) {
        StringBuilder contenidoFileTxt = new StringBuilder();
        StringBuilder listaTransacciones = new StringBuilder();
        for (ConformidadSisDatoTransaccion conformidadSisDatoTransaccion : conformidadSisDatoTransaccionList) {
            String motivoEstadoConformidad = Constantes.GUION;
            String sustentoEstadoConformidad = Constantes.GUION;
            if (conformidadSisDatoTransaccion.getIdEmpresa().equals(idEmpresaDistinct)) {
                String idTipoDocumento = (conformidadSisDatoTransaccion.getIdTipoDocumento().equals(Constantes.COD_NUMERO)) ? Constantes.COD_FACTURA : Constantes.COD_RH;
                String serieDocumento = Utilitario.obtenerNumeroSerie(conformidadSisDatoTransaccion.getSerieDocumento(), Constantes.COD_SERIE);
                String numeroDocumento = Utilitario.obtenerNumeroSerie(conformidadSisDatoTransaccion.getSerieDocumento(), Constantes.COD_NUMERO);
                Integer estado = (conformidadSisDatoTransaccion.getIdTipoConformidad().equals(Constantes.COD_CONFORMIDAD)
                        ? Constantes.COD_CONFORMIDAD : Constantes.COD_DISCONFORMIDAD);

                if (conformidadSisDatoTransaccion.getMotivoEstadoConformidad() != null &&
                        conformidadSisDatoTransaccion.getIdTipoConformidad().equals(Constantes.COD_DISCONFORMIDAD)) {
                    motivoEstadoConformidad = conformidadSisDatoTransaccion.getMotivoEstadoConformidad();
                }
                if (conformidadSisDatoTransaccion.getSustentoEstadoConformidad() != null &&
                        conformidadSisDatoTransaccion.getIdTipoConformidad().equals(Constantes.COD_DISCONFORMIDAD)) {
                    sustentoEstadoConformidad = conformidadSisDatoTransaccion.getSustentoEstadoConformidad();
                }

                contenidoFileTxt.append(
                        conformidadSisDatoTransaccion.getRucEmisor().concat(Constantes.BARRA)
                                .concat(idTipoDocumento).concat(Constantes.BARRA)
                                .concat(serieDocumento).concat(Constantes.BARRA)
                                .concat(numeroDocumento).concat(Constantes.BARRA)
                                .concat(estado.toString()).concat(Constantes.BARRA)
                                .concat(motivoEstadoConformidad).concat(Constantes.BARRA)
                                .concat(sustentoEstadoConformidad).concat(Constantes.BARRA)).append("\n");

                listaTransacciones.append(conformidadSisDatoTransaccion.getIdTransaccion() + ",");

                LOGGER.info("[getContenidoFileTxt] - Lista TMP - {}|{}|{}|{}|{}|{}|{} ",
                        conformidadSisDatoTransaccion.getRucEmisor(),
                        idTipoDocumento,
                        serieDocumento,
                        numeroDocumento,
                        estado,
                        motivoEstadoConformidad,
                        sustentoEstadoConformidad);
            }
        }

        LOGGER.info("[getContenidoFileTxt] - idEmpresa: {} - nombreFile: {} - contenido trx: {}", idEmpresaDistinct, nombreFile, listaTransacciones);
        return contenidoFileTxt;
    }

    private StringBuilder getTransaccionList(Integer tipo, Integer idEmpresaDistinct, List<ConformidadSisDatoTransaccion> conformidadSisDatoTransaccionList) {
        StringBuilder transaccionList = new StringBuilder();
        try {
            for (ConformidadSisDatoTransaccion conformidadSisDatoTransaccion : conformidadSisDatoTransaccionList) {
                if (conformidadSisDatoTransaccion.getIdEmpresa().equals(idEmpresaDistinct)) {
                    if (conformidadSisDatoTransaccion.getIdTipoConformidad().equals(tipo)) {
                        transaccionList.append(conformidadSisDatoTransaccion.getIdTransaccion() + ",");
                    } else if (tipo.equals(Constantes.COD_CONFORMIDAD_DISCONFORMIDAD)) {
                        transaccionList.append(conformidadSisDatoTransaccion.getIdTransaccion() + ",");
                    }
                }
            }
            if (transaccionList.length() == Constantes.N_CERO) {
                return null;
            }
            transaccionList.deleteCharAt(transaccionList.length() - Constantes.N_UNO);
            return transaccionList;
        } catch (Exception e) {
            LOGGER.error("[getTransaccionList] - Ocurrio un error: ", e);
            return null;
        }
    }

    private String getNombreFile(String ruc, String tipoArchivo, Integer correlativo) {

        String correlativoTexto = correlativo.toString();

        if (correlativo < Constantes.N_DIEZ) {
            correlativoTexto = Constantes.T_CERO.concat(correlativo.toString());
        }

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(Constantes.FORMAT_DATE_YYYYMMDD);
        String date = simpleDateFormat.format(new Date());

        return ruc.concat(Constantes.GUION).concat(tipoArchivo).concat(Constantes.GUION).concat(date)
                .concat(Constantes.GUION).concat(correlativoTexto);
    }
}
