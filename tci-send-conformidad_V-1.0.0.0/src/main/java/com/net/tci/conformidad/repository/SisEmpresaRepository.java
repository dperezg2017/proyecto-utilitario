package com.net.tci.conformidad.repository;

import com.net.tci.conformidad.entity.SisEmpresa;

public interface SisEmpresaRepository {

    SisEmpresa getSisEmpresaByIdEmpresa(Integer idEmpresa);

    SisEmpresa getSisEmpresaByRuc(String ruc);
}
