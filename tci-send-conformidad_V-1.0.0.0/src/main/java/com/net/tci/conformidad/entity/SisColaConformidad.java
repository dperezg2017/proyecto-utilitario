package com.net.tci.conformidad.entity;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "sis_cola_conformidad")
public class SisColaConformidad implements java.io.Serializable {

    private static final long serialVersionUID = -3490812342601791634L;

    @Id
    @Column(name = "idTransaccion", unique = true, nullable = false)
    private Long idTransaccion;

    @Column(name = "idTipoConformidad")
    private Integer idTipoConformidad;

    @Column(name = "idEmpresa")
    private Integer idEmpresa;

    @Column(name = "estado")
    private Boolean estado;

    public Long getIdTransaccion() {
        return idTransaccion;
    }

    public void setIdTransaccion(Long idTransaccion) {
        this.idTransaccion = idTransaccion;
    }

    public Integer getIdTipoConformidad() {
        return idTipoConformidad;
    }

    public void setIdTipoConformidad(Integer idTipoConformidad) {
        this.idTipoConformidad = idTipoConformidad;
    }

    public Integer getIdEmpresa() {
        return idEmpresa;
    }

    public void setIdEmpresa(Integer idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    public Boolean getEstado() {
        return estado;
    }

    public void setEstado(Boolean estado) {
        this.estado = estado;
    }
}
