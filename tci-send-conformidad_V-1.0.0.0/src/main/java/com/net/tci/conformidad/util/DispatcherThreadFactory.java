package com.net.tci.conformidad.util;

import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicLong;

public class DispatcherThreadFactory implements ThreadFactory {

    private final String prefix;
    private final AtomicLong count = new AtomicLong(1);

    public DispatcherThreadFactory(String prefix) {
        this.prefix = prefix;
    }

    @Override
    public Thread newThread(Runnable r) {
        Thread thread = new Thread(r);
        thread.setName(prefix + "-" + count.getAndIncrement());
        return thread;
    }
}
