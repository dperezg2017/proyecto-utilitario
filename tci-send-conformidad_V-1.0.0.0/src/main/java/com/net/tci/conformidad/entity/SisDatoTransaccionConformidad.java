package com.net.tci.conformidad.entity;

import javax.persistence.*;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "sis_dato_transaccion_conformidad")
public class SisDatoTransaccionConformidad {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "idTransaccion", unique = true, nullable = false)
    private Integer idTransaccion;

    @Column(name = "idEstadoConformidad")
    private Long idEstadoConformidad;

    @Column(name = "idEstadoConformidadSunat")
    private Integer idEstadoConformidadSunat;

    @Column(name = "idTicketConformidad")
    private Long idTicketConformidad;

    @Column(name = "descripcionErrorTicket")
    private String descripcionErrorTicket;

    public String getDescripcionErrorTicket() {
        return descripcionErrorTicket;
    }

    public void setDescripcionErrorTicket(String descripcionErrorTicket) {
        this.descripcionErrorTicket = descripcionErrorTicket;
    }

    public Integer getIdTransaccion() {
        return idTransaccion;
    }

    public void setIdTransaccion(Integer idTransaccion) {
        this.idTransaccion = idTransaccion;
    }

    public Long getIdEstadoConformidad() {
        return idEstadoConformidad;
    }

    public void setIdEstadoConformidad(Long idEstadoConformidad) {
        this.idEstadoConformidad = idEstadoConformidad;
    }

    public Integer getIdEstadoConformidadSunat() {
        return idEstadoConformidadSunat;
    }

    public void setIdEstadoConformidadSunat(Integer idEstadoConformidadSunat) {
        this.idEstadoConformidadSunat = idEstadoConformidadSunat;
    }

    public Long getIdTicketConformidad() {
        return idTicketConformidad;
    }

    public void setIdTicketConformidad(Long idTicketConformidad) {
        this.idTicketConformidad = idTicketConformidad;
    }
}
