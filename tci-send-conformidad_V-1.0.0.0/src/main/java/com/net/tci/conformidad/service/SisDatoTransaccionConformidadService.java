package com.net.tci.conformidad.service;

public interface SisDatoTransaccionConformidadService {

    void updateIdEstadoConformidadSunatAndIdTicketConformidad(Long idTicketConformidad, StringBuilder contTransaccionList, Integer tipo);

    void updateFilaTicketError(String sqlUpdateError);

    void updateIdEstadoConformidadConformeDisconforme(Long idTicketConformidad);
}
