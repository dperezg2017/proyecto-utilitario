package com.net.tci.conformidad.service;

import com.net.tci.conformidad.entity.SisEmpresa;

public interface SisEmpresaService {

    SisEmpresa getSisEmpresaByIdEmpresa(Integer idEmpresa);

    SisEmpresa getSisEmpresaByRuc(String obtenerRuc);
}
