package com.net.tci.conformidad.util;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class UtilProperties {

    public static Integer CANT_SIS_COLA_CONFORMIDAD_PROCESAR;
    public static Integer CANT_SIS_TICKET_CONFORMIDAD_PROCESAR;

    public static int THREAD_PROCESS_SEND_STATE = 2;
    public static int THREAD_PROCESS_CONSULT_TICKET = 2;

    public static int TIEMPO_PERMITIDO_CIERRE_HILOS;
    public static int TIEMPO_HILO;

    public static String URL_OBTENER_TICKET;
    public static String URL_CONSULTAR_TICKET;

    @Value("${app.service.conformidad.url.obtener.ticket}")
    public void setUrlObtenerTicket(String url) {
        URL_OBTENER_TICKET = url;
    }

    @Value("${app.service.conformidad.url.consultar.ticket}")
    public void setUrlConsultarTicket(String url) {
        URL_CONSULTAR_TICKET = url;
    }

    @Value("${app.service.conformidad.procesamiento.cantidad.estado}")
    public void setCantSisColaConformidadProcesar(Integer cantSisColaConformidadProcesar) {
        CANT_SIS_COLA_CONFORMIDAD_PROCESAR = cantSisColaConformidadProcesar;
    }

    @Value("${app.service.conformidad.procesamiento.cantidad.ticket}")
    public void setCantSisTicketConformidadProcesar(Integer cantSisTicketConformidadProcesar) {
        CANT_SIS_TICKET_CONFORMIDAD_PROCESAR = cantSisTicketConformidadProcesar;
    }

    @Value("${app.service.send.state.process.thread.count}")
    public void setThreadSendState(int threadToken) {
        THREAD_PROCESS_SEND_STATE = threadToken;
    }

    @Value("${app.service.consult.ticket.process.thread.count}")
    public void setThreadConsultarTicket(int threadTicket) {
        THREAD_PROCESS_CONSULT_TICKET = threadTicket;
    }

    @Value("${app.documentos.thread.time.permitido.cierre.hilos}")
    public void setTiempoPermitidoCierreHilos(int tiempoPermitidoCierreHilos) {
        TIEMPO_PERMITIDO_CIERRE_HILOS = tiempoPermitidoCierreHilos;
    }

    @Value("${app.documentos.thread.time}")
    public void setTiempoHilo(int tiempoHilo) {
        TIEMPO_HILO = tiempoHilo;
    }

}
