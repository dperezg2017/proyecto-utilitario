package com.net.tci.conformidad.task.async;

import com.net.tci.conformidad.mapeo.ConformidadTicket;
import com.net.tci.conformidad.service.ConsultTicketService;
import com.net.tci.conformidad.service.SisTicketConformidadService;
import com.net.tci.conformidad.task.Task;
import com.net.tci.conformidad.util.DispatcherThreadFactory;
import com.net.tci.conformidad.util.ThreadUtil;
import com.net.tci.conformidad.util.UtilProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

@Component
@Profile("asyncConsultarTicket")
public class AsyncConsultarTicketTask implements Task {

    private static final Logger LOGGER = LoggerFactory.getLogger(AsyncConsultarTicketTask.class);

    @Autowired
    SisTicketConformidadService sisTicketConformidadService;

    @Autowired
    ConsultTicketService consultTicketService;

    @Override
    public void executeTask() {
        LOGGER.info("[asyncConsultarTicket] - Inicio de proceso GetCDR - Async");
        List<ConformidadTicket> conformidadTicketList = listarSisTicketConformidad(UtilProperties.CANT_SIS_TICKET_CONFORMIDAD_PROCESAR);

        if (!conformidadTicketList.isEmpty()) {
            LOGGER.info("[asyncConsultarTicket] - Se procesaran {} tickets", conformidadTicketList.size());
            List<Future> estadoHilos = new LinkedList<>();
            ExecutorService executor = Executors.newFixedThreadPool(UtilProperties.THREAD_PROCESS_CONSULT_TICKET,
                    new DispatcherThreadFactory("ConsultTicketAsyncDispatcher"));


            for (int i = 0; i < conformidadTicketList.size(); i++) {
                ConformidadTicket conformidadTicket = conformidadTicketList.get(i);
                Callable<String> callableTask = () -> {
                    consultTicketService.procesarConsultTicket(conformidadTicket);
                    return "Callable OK";
                };

                Future<String> future = executor.submit(callableTask);
                estadoHilos.add(future);

                if (estadoHilos.size() >= UtilProperties.THREAD_PROCESS_CONSULT_TICKET || (i + 1) >= conformidadTicketList.size()) {
                    // LOGGER.info("[asyncConsultarTicket] - Se ha enviado el total de hilos para Comprobantes getCDR, se va a esperar que se liberen los hilos");
                    if (ThreadUtil.waitForFinalizeThreads(executor, estadoHilos, UtilProperties.THREAD_PROCESS_CONSULT_TICKET)) {
                        estadoHilos.clear();
                        //    LOGGER.info("[asyncConsultarTicket] - Se han liberado todos los hilos... se siguen repartiendo las tareas.");
                    } else {
                        estadoHilos.clear();
                        //      LOGGER.info("[asyncConsultarTicket] - Ocurrio un error en el tiempo de espera de las tareas.");
                        break;
                    }
                }
            }
            // LOGGER.info("[asyncConsultarTicket] - Esperando finalizar el bloque de transacciones obtenidas");
            if (ThreadUtil.waitForCloseThreads(executor, UtilProperties.THREAD_PROCESS_CONSULT_TICKET)) {
                //     LOGGER.info("[asyncConsultarTicket] - Hilos finalizados correctamente");
            }
        }
        LOGGER.info("[asyncConsultarTicket] - Fin de proceso GetCdr - Async");
    }


    private List<ConformidadTicket> listarSisTicketConformidad(Integer cantidad) {
        List<ConformidadTicket> conformidadTicketList = new ArrayList<>();
        try {
            conformidadTicketList = sisTicketConformidadService.getTicketConformidad(cantidad);
            return conformidadTicketList;
        } catch (Exception e) {
            LOGGER.error("[listarConformidadSisDatoTransaccion] - Ocurrio un error: ", e);
            return conformidadTicketList;
        }
    }

}
