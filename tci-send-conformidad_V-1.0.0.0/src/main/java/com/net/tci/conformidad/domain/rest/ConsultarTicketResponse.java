package com.net.tci.conformidad.domain.rest;

public class ConsultarTicketResponse {

    private String codEstadoProceso;
    private String desArchivo;
    private String valHash;
    private String fecRegis;
    private Integer cntRegistrosCpe;
    private Integer cntRegistrosCorrectos;
    private Integer cntRegistrosErrores;
    private String arcErrores;

    public String getCodEstadoProceso() {
        return codEstadoProceso;
    }

    public void setCodEstadoProceso(String codEstadoProceso) {
        this.codEstadoProceso = codEstadoProceso;
    }

    public String getDesArchivo() {
        return desArchivo;
    }

    public void setDesArchivo(String desArchivo) {
        this.desArchivo = desArchivo;
    }

    public String getValHash() {
        return valHash;
    }

    public void setValHash(String valHash) {
        this.valHash = valHash;
    }

    public String getFecRegis() {
        return fecRegis;
    }

    public void setFecRegis(String fecRegis) {
        this.fecRegis = fecRegis;
    }

    public Integer getCntRegistrosCpe() {
        return cntRegistrosCpe;
    }

    public void setCntRegistrosCpe(Integer cntRegistrosCpe) {
        this.cntRegistrosCpe = cntRegistrosCpe;
    }

    public Integer getCntRegistrosCorrectos() {
        return cntRegistrosCorrectos;
    }

    public void setCntRegistrosCorrectos(Integer cntRegistrosCorrectos) {
        this.cntRegistrosCorrectos = cntRegistrosCorrectos;
    }

    public Integer getCntRegistrosErrores() {
        return cntRegistrosErrores;
    }

    public void setCntRegistrosErrores(Integer cntRegistrosErrores) {
        this.cntRegistrosErrores = cntRegistrosErrores;
    }

    public String getArcErrores() {
        return arcErrores;
    }

    public void setArcErrores(String arcErrores) {
        this.arcErrores = arcErrores;
    }
}
