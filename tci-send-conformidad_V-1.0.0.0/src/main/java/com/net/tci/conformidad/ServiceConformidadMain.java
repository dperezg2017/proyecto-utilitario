package com.net.tci.conformidad;

import com.net.tci.conformidad.util.Constantes;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.GenericXmlApplicationContext;

import java.io.IOException;
import java.util.Properties;

public class ServiceConformidadMain {

    private static final Logger LOGGER = LoggerFactory.getLogger(ServiceConformidadMain.class);

    public static GenericXmlApplicationContext activatedProfile
            (GenericXmlApplicationContext applicationContext, String estado, String beanActivated, String beanDefault) {
        switch (estado) {
            case Constantes.T_UNO:
                applicationContext.getEnvironment().addActiveProfile(beanActivated);
                break;
            case Constantes.T_CERO:
            default:
                applicationContext.getEnvironment().addActiveProfile(beanDefault);
                break;
        }
        return applicationContext;
    }

    public static void main(String[] args) {
        Properties p = new Properties();
        try {
            p.load(ClassLoader.getSystemResourceAsStream("app.properties"));
        } catch (IOException e) {
            LOGGER.error("[main] - Ocurrio un error al obtener el app.properties {}", e.getMessage());
        }
        GenericXmlApplicationContext applicationContext = new GenericXmlApplicationContext();

        if (p.getProperty("app.thread.activate.service.conformidad").equals(Constantes.T_UNO)) {
            applicationContext = ServiceConformidadMain.activatedProfile(applicationContext, p.getProperty("app.service.conformidad.procesamiento.estado"), "asyncSendState", "syncSendState");
        }
        if (p.getProperty("app.thread.activate.service.eliminar.correlativo").equals(Constantes.T_UNO)) {
            applicationContext = ServiceConformidadMain.activatedProfile(applicationContext, p.getProperty("app.service.conformidad.procesamiento.elimina.correlativo"), "asyncEliminaCorrelativo", "syncEliminaCorrelativo");
        }
        if (p.getProperty("app.thread.activate.service.consult.ticket").equals(Constantes.T_UNO)) {
            applicationContext = ServiceConformidadMain.activatedProfile(applicationContext, p.getProperty("app.service.conformidad.procesamiento.consult.ticket"), "asyncConsultarTicket", "syncConsultarTicket");
        }

        p = null;
        applicationContext.load("spring/applicationContext.xml");
        applicationContext.refresh();

    }

}
