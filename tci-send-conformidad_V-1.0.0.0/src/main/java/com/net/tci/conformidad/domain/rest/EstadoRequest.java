package com.net.tci.conformidad.domain.rest;

import org.springframework.web.multipart.MultipartFile;

public class EstadoRequest {

    private MultipartFile archivo;
    private String valHash;

    public MultipartFile getArchivo() {
        return archivo;
    }

    public void setArchivo(MultipartFile archivo) {
        this.archivo = archivo;
    }

    public String getValHash() {
        return valHash;
    }

    public void setValHash(String valHash) {
        this.valHash = valHash;
    }
}
