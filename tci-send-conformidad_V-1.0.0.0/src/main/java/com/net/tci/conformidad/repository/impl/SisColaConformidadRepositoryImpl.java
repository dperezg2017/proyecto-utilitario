package com.net.tci.conformidad.repository.impl;

import com.net.tci.conformidad.entity.SisColaConformidad;
import com.net.tci.conformidad.mapeo.ConformidadSisDatoTransaccion;
import com.net.tci.conformidad.repository.HibernateRepository;
import com.net.tci.conformidad.repository.SisColaConformidadRepository;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.transform.Transformers;
import org.hibernate.type.BooleanType;
import org.hibernate.type.IntegerType;
import org.hibernate.type.LongType;
import org.hibernate.type.StringType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public class SisColaConformidadRepositoryImpl extends HibernateRepository<SisColaConformidad, Long> implements SisColaConformidadRepository {

    private static final Logger LOGGER = LoggerFactory.getLogger(SisColaConformidadRepositoryImpl.class);

    @Transactional(readOnly = true)
    public List<ConformidadSisDatoTransaccion> getListaConformidadSisDatoTransaccion(Integer cantidad) {

        try {
//            final String sql = "SELECT " +
//                    "scc.idTransaccion AS idTransaccion," +
//                    "scc.idTipoConformidad AS idTipoConformidad," +
//                    "scc.idEmpresa AS idEmpresa, " +
//                    "scc.estado AS estado," +
//                    "sdt.ruc_emisor AS rucEmisor, " +
//                    "sdt.idTipoDocumento AS idTipoDocumento," +
//                    "sdt.serie_documento AS serieDocumento " +
//                    "FROM sis_dato_transaccion sdt " +
//                    "INNER JOIN sis_cola_conformidad scc " +
//                    "ON sdt.idTransaccion=scc.idTransaccion WHERE scc.estado = 1";
            final String sql = "SELECT " +
                    "scc.idTransaccion AS idTransaccion, " +
                    "scc.idTipoConformidad AS idTipoConformidad, " +
                    "scc.idEmpresa AS idEmpresa, " +
                    "scc.estado AS estado, " +
                    "sdt.ruc_emisor AS rucEmisor, " +
                    "sdt.idTipoDocumento AS idTipoDocumento, " +
                    "sdt.serie_documento AS serieDocumento, " +
                    "motivoEstadoConformidad AS motivoEstadoConformidad, " +
                    "sustentoEstadoConformidad AS sustentoEstadoConformidad " +
                    "FROM sis_dato_transaccion sdt " +
                    "INNER JOIN sis_cola_conformidad scc " +
                    "ON sdt.idTransaccion=scc.idTransaccion " +
                    "INNER JOIN sis_dato_transaccion_conformidad sdtc " +
                    "ON sdtc.idTransaccion = scc.idTransaccion " +
                    "WHERE scc.estado = 1 ";
            Session session = sessionFactory.getCurrentSession();

            List<ConformidadSisDatoTransaccion> conformidadSisDatoTransaccionList = session.createSQLQuery(sql)
                    .addScalar("idTransaccion", LongType.INSTANCE)
                    .addScalar("idTipoConformidad", IntegerType.INSTANCE)
                    .addScalar("idEmpresa", IntegerType.INSTANCE)
                    .addScalar("estado", BooleanType.INSTANCE)
                    .addScalar("rucEmisor", StringType.INSTANCE)
                    .addScalar("idTipoDocumento", IntegerType.INSTANCE)
                    .addScalar("serieDocumento", StringType.INSTANCE)
                    .addScalar("motivoEstadoConformidad", StringType.INSTANCE)
                    .addScalar("sustentoEstadoConformidad", StringType.INSTANCE)
                    .setMaxResults(cantidad)
                    .setResultTransformer(Transformers.aliasToBean(ConformidadSisDatoTransaccion.class)).list();
            return conformidadSisDatoTransaccionList;
        } catch (Exception e) {
            LOGGER.error("[getListaConformidadSisDatoTransaccion] - Ocurrio un error: ", e);
            return null;
        }
    }


    @Transactional
    public void updateColaConformidadByIntento(StringBuilder listaTransacciones) {
        try {
            String hql = "UPDATE sis_cola_conformidad SET reintento=reintento+1 WHERE idTransaccion IN (" + listaTransacciones + ")";
            Session session = sessionFactory.getCurrentSession();
            Query query = session.createSQLQuery(hql);
            query.executeUpdate();
        } catch (Exception e) {
            LOGGER.error("[updateColaConformidadByIntento] - Ocurrio un error: ", e);
        }
    }

    @Transactional
    public void deleteColaConformidadByTransaccion(StringBuilder listaTransacciones) {
        try {
            String hql = "DELETE FROM sis_cola_conformidad WHERE idTransaccion IN (" + listaTransacciones + ")";
            Session session = sessionFactory.getCurrentSession();
            Query query = session.createSQLQuery(hql);
            query.executeUpdate();
        } catch (Exception e) {
            LOGGER.error("[updateColaConformidadByIntento] - Ocurrio un error: ", e);
        }
    }

}
