package com.net.tci.conformidad.repository;

public interface SisDatoTransaccionConformidadRepository {

    void updateIdEstadoConformidadSunatAndIdTicketConformidad(Long idTicketConformidad, StringBuilder contTransaccionList, Integer tipo);

    void updateFilaTicketError(String sqlUpdateError);

    void updateIdEstadoConformidadConformeDisconforme(Long idTicketConformidad);
}
