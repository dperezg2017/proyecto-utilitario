package com.net.tci.conformidad.service.impl;

import com.net.tci.conformidad.repository.SisDatoTransaccionConformidadRepository;
import com.net.tci.conformidad.service.SisDatoTransaccionConformidadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SisDatoTransaccionConformidadServiceImpl implements SisDatoTransaccionConformidadService {

    @Autowired
    SisDatoTransaccionConformidadRepository sisDatoTransaccionConformidadRepository;

    public void updateIdEstadoConformidadSunatAndIdTicketConformidad(Long idTicketConformidad, StringBuilder contTransaccionList, Integer tipo) {
        sisDatoTransaccionConformidadRepository.updateIdEstadoConformidadSunatAndIdTicketConformidad(idTicketConformidad, contTransaccionList, tipo);
    }

    @Override
    public void updateFilaTicketError(String sqlUpdateError) {
        sisDatoTransaccionConformidadRepository.updateFilaTicketError(sqlUpdateError);
    }

    @Override
    public void updateIdEstadoConformidadConformeDisconforme(Long idTicketConformidad) {
        sisDatoTransaccionConformidadRepository.updateIdEstadoConformidadConformeDisconforme(idTicketConformidad);
    }
}
