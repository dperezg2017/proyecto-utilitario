package com.net.tci.conformidad.entity;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "sis_dato_transaccion")
public class SisDatoTransaccion implements java.io.Serializable {

    private static final long serialVersionUID = 6679057881143163450L;
    private Long idTransaccion;
    private Date fechaTransaccion;
    private Integer idTipoDocumento;
    private String nombreDocumento;
    private String serieDocumento;
    private String fechaEmision;
    private Integer idEmisor;
    private String rucEmisor;
    private String razonSocialEmisor;
    private Integer idReceptor;
    private String rucReceptor;
    private String razonSocialReceptor;
    private Integer idCasillaOrigen;
    private String casillaOrigen;
    private String tipoDocumento;
    private String importeAPagar;
    private String igvImporteTotal;
    private String montoTotalAPagar;
    private String moneda;
    private String alias;
    private String tipoDocumentoReceptor;
    private String montoDescuento;
    private String documentoReferencia;
    private String observacion;
    private String correoElectronicoReceptor;
    private String jsonEmail;
    private Boolean estadoReversado;
    private Boolean estadoOtorgado;
    private Date fechaOtorgado;
    private Boolean estadoAnulado;
    private Boolean estadoRechazado;
    private Boolean estadoLeido;
    private Integer estadoRespuesta;
    private Integer estadoRespuestaSunat;
    private Boolean estadoAdjunto;
    private Boolean estadoCorreo;
    private String numeroTicket;
    private Boolean estadoExterno;
    private Date fechaEmisionConvert;


    public SisDatoTransaccion() {
    }

    public SisDatoTransaccion(Long idTransaccion,
                              String serieDocumento,
                              String fechaEmision,
                              String rucEmisor,
                              String razonSocialEmisor,
                              String rucReceptor,
                              String razonSocialReceptor,
                              String tipoDocumento,
                              String importeAPagar,
                              String igvImporteTotal,
                              String montoTotalAPagar,
                              String moneda) {
        this.idTransaccion = idTransaccion;
        this.serieDocumento = serieDocumento;
        this.fechaEmision = fechaEmision;
        this.rucEmisor = rucEmisor;
        this.razonSocialEmisor = razonSocialEmisor;
        this.rucReceptor = rucReceptor;
        this.razonSocialReceptor = razonSocialReceptor;
        this.tipoDocumento = tipoDocumento;
        this.importeAPagar = importeAPagar;
        this.igvImporteTotal = igvImporteTotal;
        this.montoTotalAPagar = montoTotalAPagar;
        this.moneda = moneda;
    }

    @GenericGenerator(name = "generator", strategy = "foreign", parameters = @Parameter(name = "property", value = "transaccion"))
    @Id
    @GeneratedValue(generator = "generator")
    @Column(name = "idTransaccion", unique = true, nullable = false)
    public Long getIdTransaccion() {
        return this.idTransaccion;
    }

    public void setIdTransaccion(Long idTransaccion) {
        this.idTransaccion = idTransaccion;
    }

    @Column(name = "serie_documento", length = 20)
    public String getSerieDocumento() {
        return this.serieDocumento;
    }

    public void setSerieDocumento(String serieDocumento) {
        this.serieDocumento = serieDocumento;
    }

    @Column(name = "fecha_emision", length = 10)
    public String getFechaEmision() {
        return this.fechaEmision;
    }

    public void setFechaEmision(String fechaEmision) {
        this.fechaEmision = fechaEmision;
    }

    @Column(name = "ruc_emisor", length = 15)
    public String getRucEmisor() {
        return this.rucEmisor;
    }

    public void setRucEmisor(String rucEmisor) {
        this.rucEmisor = rucEmisor;
    }

    @Column(name = "razon_social_emisor", length = 100)
    public String getRazonSocialEmisor() {
        return this.razonSocialEmisor;
    }

    public void setRazonSocialEmisor(String razonSocialEmisor) {
        this.razonSocialEmisor = razonSocialEmisor;
    }

    @Column(name = "ruc_receptor", length = 15)
    public String getRucReceptor() {
        return this.rucReceptor;
    }

    public void setRucReceptor(String rucReceptor) {
        this.rucReceptor = rucReceptor;
    }

    @Column(name = "razon_social_receptor")
    public String getRazonSocialReceptor() {
        return this.razonSocialReceptor;
    }

    public void setRazonSocialReceptor(String razonSocialReceptor) {
        this.razonSocialReceptor = razonSocialReceptor;
    }

    @Column(name = "tipo_documento", length = 50)
    public String getTipoDocumento() {
        return this.tipoDocumento;
    }

    public void setTipoDocumento(String tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

    @Column(name = "importe_a_pagar", length = 25)
    public String getImporteAPagar() {
        return this.importeAPagar;
    }

    public void setImporteAPagar(String importeAPagar) {
        this.importeAPagar = importeAPagar;
    }

    @Column(name = "igv_importe_total", length = 15)
    public String getIgvImporteTotal() {
        return this.igvImporteTotal;
    }

    public void setIgvImporteTotal(String igvImporteTotal) {
        this.igvImporteTotal = igvImporteTotal;
    }

    @Column(name = "monto_total_a_pagar", length = 15)
    public String getMontoTotalAPagar() {
        return this.montoTotalAPagar;
    }

    public void setMontoTotalAPagar(String montoTotalAPagar) {
        this.montoTotalAPagar = montoTotalAPagar;
    }

    @Column(name = "moneda", length = 3)
    public String getMoneda() {
        return this.moneda;
    }

    public void setMoneda(String moneda) {
        this.moneda = moneda;
    }

    @Column(name = "alias")
    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    @Column(name = "tipo_documento_receptor")
    public String getTipoDocumentoReceptor() {
        return tipoDocumentoReceptor;
    }

    public void setTipoDocumentoReceptor(String tipoDocumentoReceptor) {
        this.tipoDocumentoReceptor = tipoDocumentoReceptor;
    }

    @Column(name = "monto_descuento")
    public String getMontoDescuento() {
        return montoDescuento;
    }

    public void setMontoDescuento(String montoDescuento) {
        this.montoDescuento = montoDescuento;
    }

    @Column(name = "documento_referencia")
    public String getDocumentoReferencia() {
        return documentoReferencia;
    }

    public void setDocumentoReferencia(String documentoReferencia) {
        this.documentoReferencia = documentoReferencia;
    }

    @Column(name = "descripcion")
    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    @Column(name = "correo_electronico_receptor")
    public String getCorreoElectronicoReceptor() {
        return correoElectronicoReceptor;
    }

    public void setCorreoElectronicoReceptor(String correoElectronicoReceptor) {
        this.correoElectronicoReceptor = correoElectronicoReceptor;
    }

    @Column(name = "json_email")
    public String getJsonEmail() {
        return jsonEmail;
    }

    public void setJsonEmail(String jsonEmail) {
        this.jsonEmail = jsonEmail;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "fecha_transaccion")
    public Date getFechaTransaccion() {
        return fechaTransaccion;
    }

    public void setFechaTransaccion(Date fechaTransaccion) {
        this.fechaTransaccion = fechaTransaccion;
    }

    @Column(name = "idTipoDocumento")
    public Integer getIdTipoDocumento() {
        return idTipoDocumento;
    }

    public void setIdTipoDocumento(Integer idTipoDocumento) {
        this.idTipoDocumento = idTipoDocumento;
    }

    @Column(name = "nombre_documento")
    public String getNombreDocumento() {
        return nombreDocumento;
    }

    public void setNombreDocumento(String nombreDocumento) {
        this.nombreDocumento = nombreDocumento;
    }

    @Column(name = "id_emisor")
    public Integer getIdEmisor() {
        return idEmisor;
    }

    public void setIdEmisor(Integer idEmisor) {
        this.idEmisor = idEmisor;
    }

    @Column(name = "id_receptor")
    public Integer getIdReceptor() {
        return idReceptor;
    }

    public void setIdReceptor(Integer idReceptor) {
        this.idReceptor = idReceptor;
    }

    @Column(name = "idCasillaOrigen")
    public Integer getIdCasillaOrigen() {
        return idCasillaOrigen;
    }

    public void setIdCasillaOrigen(Integer idCasillaOrigen) {
        this.idCasillaOrigen = idCasillaOrigen;
    }

    @Column(name = "casillaOrigen")
    public String getCasillaOrigen() {
        return casillaOrigen;
    }

    public void setCasillaOrigen(String casillaOrigen) {
        this.casillaOrigen = casillaOrigen;
    }

    @Column(name = "estadoReversado")
    public Boolean getEstadoReversado() {
        return estadoReversado;
    }

    public void setEstadoReversado(Boolean estadoReversado) {
        this.estadoReversado = estadoReversado;
    }

    @Column(name = "estadoOtorgado")
    public Boolean getEstadoOtorgado() {
        return estadoOtorgado;
    }

    public void setEstadoOtorgado(Boolean estadoOtorgado) {
        this.estadoOtorgado = estadoOtorgado;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "fecha_otorgado")
    public Date getFechaOtorgado() {
        return fechaOtorgado;
    }

    public void setFechaOtorgado(Date fechaOtorgado) {
        this.fechaOtorgado = fechaOtorgado;
    }

    @Column(name = "estadoAnulado")
    public Boolean getEstadoAnulado() {
        return estadoAnulado;
    }

    public void setEstadoAnulado(Boolean estadoAnulado) {
        this.estadoAnulado = estadoAnulado;
    }

    @Column(name = "estadoRechazado")
    public Boolean getEstadoRechazado() {
        return estadoRechazado;
    }

    public void setEstadoRechazado(Boolean estadoRechazado) {
        this.estadoRechazado = estadoRechazado;
    }

    @Column(name = "estadoLeido")
    public Boolean getEstadoLeido() {
        return estadoLeido;
    }

    public void setEstadoLeido(Boolean estadoLeido) {
        this.estadoLeido = estadoLeido;
    }

    @Column(name = "estadoRespuesta")
    public Integer getEstadoRespuesta() {
        return estadoRespuesta;
    }

    public void setEstadoRespuesta(Integer estadoRespuesta) {
        this.estadoRespuesta = estadoRespuesta;
    }

    @Column(name = "estadoRespuestaSunat")
    public Integer getEstadoRespuestaSunat() {
        return estadoRespuestaSunat;
    }

    public void setEstadoRespuestaSunat(Integer estadoRespuestaSunat) {
        this.estadoRespuestaSunat = estadoRespuestaSunat;
    }


    @Column(name = "estadoAdjunto")
    public Boolean getEstadoAdjunto() {
        return estadoAdjunto;
    }

    public void setEstadoAdjunto(Boolean estadoAdjunto) {
        this.estadoAdjunto = estadoAdjunto;
    }

    @Column(name = "estadoCorreo")
    public Boolean getEstadoCorreo() {
        return estadoCorreo;
    }

    public void setEstadoCorreo(Boolean estadoCorreo) {
        this.estadoCorreo = estadoCorreo;
    }

    @Column(name = "numeroTicket")
    public String getNumeroTicket() {
        return numeroTicket;
    }

    public void setNumeroTicket(String numeroTicket) {
        this.numeroTicket = numeroTicket;
    }

    public Boolean getEstadoExterno() {
        return estadoExterno;
    }

    public void setEstadoExterno(Boolean estadoExterno) {
        this.estadoExterno = estadoExterno;
    }

    public Date getFechaEmisionConvert() {
        return fechaEmisionConvert;
    }

    public void setFechaEmisionConvert(Date fechaEmisionConvert) {
        this.fechaEmisionConvert = fechaEmisionConvert;
    }
}
