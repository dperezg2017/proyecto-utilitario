package com.net.tci.conformidad.task.async;

import com.net.tci.conformidad.mapeo.ConformidadSisDatoTransaccion;
import com.net.tci.conformidad.service.SendStateService;
import com.net.tci.conformidad.service.SisColaConformidadService;
import com.net.tci.conformidad.task.Task;
import com.net.tci.conformidad.util.Constantes;
import com.net.tci.conformidad.util.DispatcherThreadFactory;
import com.net.tci.conformidad.util.ThreadUtil;
import com.net.tci.conformidad.util.UtilProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.stream.Collectors;

@Component
@Profile("asyncSendState")
public class AsyncSendStateTask implements Task {

    private static final Logger LOGGER = LoggerFactory.getLogger(AsyncSendStateTask.class);

    @Autowired
    SisColaConformidadService sisColaConformidadService;

    @Autowired
    SendStateService getStateService;

    @Override
    public void executeTask() {
        long timeStart = System.currentTimeMillis();
        LOGGER.info("[asyncSendState] - Inicio de proceso de AsyncSendState: {}", new Date());
        List<ConformidadSisDatoTransaccion> conformidadSisDatoTransaccionList = listarConformidadSisDatoTransaccion(UtilProperties.CANT_SIS_COLA_CONFORMIDAD_PROCESAR);
        List<Integer> idEmpresaList = new ArrayList<>();

        for (ConformidadSisDatoTransaccion conformidadSisDatoTransaccion : conformidadSisDatoTransaccionList) {
            idEmpresaList.add(conformidadSisDatoTransaccion.getIdEmpresa());
        }
        List<Integer> idEmpresaDistinctList = idEmpresaList.stream().distinct().collect(Collectors.toList());

        if (!idEmpresaDistinctList.isEmpty()) {
            LOGGER.info("[asyncSendState] - Se procesaran {} transacciones", conformidadSisDatoTransaccionList.size());
            List<Future> estadoHilos = new LinkedList<>();
            ExecutorService executor = Executors.newFixedThreadPool(UtilProperties.THREAD_PROCESS_SEND_STATE,
                    new DispatcherThreadFactory("SendStateAsyncDispatcher"));

            for (int i = Constantes.N_CERO; i < idEmpresaDistinctList.size(); i++) {
                Integer idEmpresaDistinct = idEmpresaDistinctList.get(i);
                Callable<String> callableTask = () -> {
                    getStateService.procesarSendState(idEmpresaDistinct, conformidadSisDatoTransaccionList);
                    return "Callable OK";
                };

                Future<String> future = executor.submit(callableTask);
                estadoHilos.add(future);

                if (estadoHilos.size() >= UtilProperties.THREAD_PROCESS_CONSULT_TICKET || (i + 1) >= idEmpresaDistinctList.size()) {
                    //    LOGGER.info("[asyncSendState] - Se ha enviado el total de hilos para Comprobantes getCDR, se va a esperar que se liberen los hilos");
                    if (ThreadUtil.waitForFinalizeThreads(executor, estadoHilos, UtilProperties.THREAD_PROCESS_CONSULT_TICKET)) {
                        estadoHilos.clear();
                       // LOGGER.info("[asyncSendState] - Se han liberado todos los hilos... se siguen repartiendo las tareas.");
                    } else {
                        estadoHilos.clear();
                        //  LOGGER.info("[asyncSendState] - Ocurrio un error en el tiempo de espera de las tareas.");
                        break;
                    }
                }
            }
            // LOGGER.info("[asyncSendState] - Esperando finalizar el bloque de transacciones obtenidas");
            if (ThreadUtil.waitForCloseThreads(executor, UtilProperties.THREAD_PROCESS_CONSULT_TICKET)) {
                //   LOGGER.info("[asyncSendState] - Hilos finalizados correctamente");
            }
        }

        LOGGER.info("[asyncSendState] - Tiempo utilizado de proceso: {} milliseconds.", (System.currentTimeMillis() - timeStart));
    }

    private List<ConformidadSisDatoTransaccion> listarConformidadSisDatoTransaccion(Integer cantidad) {
        List<ConformidadSisDatoTransaccion> conformidadSisDatoTransaccionList = new ArrayList<>();
        try {
            conformidadSisDatoTransaccionList = sisColaConformidadService.getListaConformidadSisDatoTransaccion(cantidad);
            return conformidadSisDatoTransaccionList;
        } catch (Exception e) {
            LOGGER.error("[listarConformidadSisDatoTransaccion] - Ocurrio un error: ", e);
            return conformidadSisDatoTransaccionList;
        }
    }

}
