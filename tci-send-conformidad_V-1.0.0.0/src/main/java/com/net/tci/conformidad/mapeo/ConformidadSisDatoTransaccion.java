package com.net.tci.conformidad.mapeo;

public class ConformidadSisDatoTransaccion implements java.io.Serializable {

    private static final long serialVersionUID = -3490812342601791634L;

    private Long idTransaccion;

    private Integer idTipoConformidad;

    private Integer idEmpresa;

    private Boolean estado;

    private String rucEmisor;

    private Integer idTipoDocumento;

    private String serieDocumento;

    private String motivoEstadoConformidad;

    private String sustentoEstadoConformidad;

    public String getMotivoEstadoConformidad() {
        return motivoEstadoConformidad;
    }

    public void setMotivoEstadoConformidad(String motivoEstadoConformidad) {
        this.motivoEstadoConformidad = motivoEstadoConformidad;
    }

    public String getSustentoEstadoConformidad() {
        return sustentoEstadoConformidad;
    }

    public void setSustentoEstadoConformidad(String sustentoEstadoConformidad) {
        this.sustentoEstadoConformidad = sustentoEstadoConformidad;
    }

    public Long getIdTransaccion() {
        return idTransaccion;
    }

    public void setIdTransaccion(Long idTransaccion) {
        this.idTransaccion = idTransaccion;
    }

    public Integer getIdTipoConformidad() {
        return idTipoConformidad;
    }

    public void setIdTipoConformidad(Integer idTipoConformidad) {
        this.idTipoConformidad = idTipoConformidad;
    }

    public Integer getIdEmpresa() {
        return idEmpresa;
    }

    public void setIdEmpresa(Integer idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    public Boolean getEstado() {
        return estado;
    }

    public void setEstado(Boolean estado) {
        this.estado = estado;
    }

    public String getRucEmisor() {
        return rucEmisor;
    }

    public void setRucEmisor(String rucEmisor) {
        this.rucEmisor = rucEmisor;
    }

    public Integer getIdTipoDocumento() {
        return idTipoDocumento;
    }

    public void setIdTipoDocumento(Integer idTipoDocumento) {
        this.idTipoDocumento = idTipoDocumento;
    }

    public String getSerieDocumento() {
        return serieDocumento;
    }

    public void setSerieDocumento(String serieDocumento) {
        this.serieDocumento = serieDocumento;
    }
}
