package com.net.tci.conformidad.service.impl;

import com.net.tci.conformidad.entity.SisTicketConformidad;
import com.net.tci.conformidad.mapeo.ConformidadTicket;
import com.net.tci.conformidad.repository.SisTicketConformidadRepository;
import com.net.tci.conformidad.service.SisTicketConformidadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SisTicketConformidadImpl implements SisTicketConformidadService {

    @Autowired
    SisTicketConformidadRepository sisTicketConformidadRepository;

    public Long saveSisTicketConformidad(SisTicketConformidad sisTicketConformidad) {
        return sisTicketConformidadRepository.saveSisTicketConformidad(sisTicketConformidad);
    }

    @Override
    public List<ConformidadTicket> getTicketConformidad(Integer cantidad) {
        return sisTicketConformidadRepository.getTicketConformidad(cantidad);
    }

    @Override
    public void updateEstadoDeshabilitado(Long idTicketConformidad) {
        sisTicketConformidadRepository.updateEstadoDeshabilitado(idTicketConformidad);
    }

}
