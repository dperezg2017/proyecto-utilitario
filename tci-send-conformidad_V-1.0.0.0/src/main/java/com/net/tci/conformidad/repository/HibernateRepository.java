package com.net.tci.conformidad.repository;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

public class HibernateRepository<T, I extends Serializable> implements GenericRepository<T, I> {

    @Autowired
    protected SessionFactory sessionFactory;

    @Override
    public void add(T t) {
        Session session = sessionFactory.getCurrentSession();
        session.save(t);
    }

    @Override
    public void addCommit(T t) {
        Session session = sessionFactory.getCurrentSession();
        session.save(t);
        Transaction tx = session.beginTransaction();
        if (!tx.wasCommitted()) {
            tx.commit();
        }

        session.getTransaction().commit();
    }

    @Override
    public boolean addFlush(T t) {
        //Session session = sessionFactory.getCurrentSession();
        Session session = sessionFactory.openSession();
        long count = (Long) session.save(t);
        session.getTransaction().commit();
        if (count > 0) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean addFlushString(T t) {
        SessionFactory sf = new Configuration().configure().buildSessionFactory();
        Session session = sf.openSession();
//        Session session = sessionFactory.getCurrentSession();
        Object count = (Object) session.save(t);
        session.getTransaction().commit();
        if (count != null) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void update(T t) {
        sessionFactory.getCurrentSession().update(t);
    }

    @Override
    public void delete(T t) {
        Session session = sessionFactory.getCurrentSession();
        session.delete(t);
    }

    @Override
    public T findById(I id) {
        Session session = sessionFactory.getCurrentSession();
        return (T) session.get(getEntityClass(), id);
    }

    @Override
    public List<T> findAll() {
        Session session = sessionFactory.getCurrentSession();
        return session.createCriteria(getEntityClass()).list();
    }

    @Override
    public void saveOrUpdate(T t) {
        Session session = sessionFactory.getCurrentSession();
        session.saveOrUpdate(t);
    }

    private Class<T> getEntityClass() {
        return (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
    }

    public void addCollection(Collection<T> lstEntidad) {
        try {
            Iterator<T> it = lstEntidad.iterator();
            while (it.hasNext()) {
                add(it.next());
            }
        } catch (RuntimeException re) {
            throw re;
        }

    }

}
