package com.net.tci.conformidad.repository.impl;

import com.net.tci.conformidad.entity.SisCorrelativoConformidad;
import com.net.tci.conformidad.repository.HibernateRepository;
import com.net.tci.conformidad.repository.SisCorrelativoConformidadRepository;
import org.hibernate.Query;
import org.hibernate.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public class SisCorrelativoConformidadRepositoryImpl extends HibernateRepository<SisCorrelativoConformidad, Long>
        implements SisCorrelativoConformidadRepository {

    private static final Logger LOGGER = LoggerFactory.getLogger(SisCorrelativoConformidadRepositoryImpl.class);

    @Transactional(readOnly = true)
    public SisCorrelativoConformidad getSisCorrelativoConformidad(Integer idEmpresa) {
        try {
            String hql = "from SisCorrelativoConformidad e where e.idEmpresa =:idEmpresa";
            Session session = sessionFactory.getCurrentSession();
            Query query = session.createQuery(hql);
            query.setParameter("idEmpresa", idEmpresa);
            return (SisCorrelativoConformidad) query.uniqueResult();
        } catch (Exception e) {
            LOGGER.error("[getSisCorrelativoConformidad] - Ocurrio un error: ", e);
            return null;
        }
    }

    @Transactional
    public void updateSisCorrelativoConformidadByDis(Integer idEmpresa) {
        try {
            String hql = "UPDATE sis_correlativo_conformidad SET correlativoDis=correlativoDis+1 WHERE idEmpresa=:idEmpresa";
            Session session = sessionFactory.getCurrentSession();
            Query query = session.createSQLQuery(hql);
            query.setParameter("idEmpresa", idEmpresa);
            query.executeUpdate();
        } catch (Exception e) {
            LOGGER.error("[updateSisCorrelativoConformidadByDis] - Ocurrio un error: ", e);
        }
    }

    @Transactional
    public void insertSisCorrelativoConformidad(Integer idEmpresa) {
        try {
            String hql = "INSERT INTO sis_correlativo_conformidad (idEmpresa) values (:idEmpresa)";
            Session session = sessionFactory.getCurrentSession();
            Query query = session.createSQLQuery(hql);
            query.setParameter("idEmpresa", idEmpresa);
            query.executeUpdate();
        } catch (Exception e) {
            LOGGER.error("[insertSisCorrelativoConformidad] - Ocurrio un error: ", e);
        }
    }

    @Transactional
    public void eliminarSisCorrelativoConformidad() {
        try {
            String hql = "DELETE FROM sis_correlativo_conformidad ";
            Session session = sessionFactory.getCurrentSession();
            Query query = session.createSQLQuery(hql);
            query.executeUpdate();
        } catch (Exception e) {
            LOGGER.error("[eliminarSisCorrelativoConformidad] - Ocurrio un error: ", e);
        }
    }
}
