package com.net.tci.conformidad.mapeo;

public class ConformidadIdEmpresa {

    private Integer idEmpresaFiltro;


    public Integer getIdEmpresaFiltro() {
        return idEmpresaFiltro;
    }

    public void setIdEmpresaFiltro(Integer idEmpresaFiltro) {
        this.idEmpresaFiltro = idEmpresaFiltro;
    }
}
