package com.net.tci.conformidad.repository.impl;

import com.net.tci.conformidad.entity.SisTicketConformidad;
import com.net.tci.conformidad.mapeo.ConformidadTicket;
import com.net.tci.conformidad.repository.HibernateRepository;
import com.net.tci.conformidad.repository.SisTicketConformidadRepository;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.transform.Transformers;
import org.hibernate.type.BooleanType;
import org.hibernate.type.DateType;
import org.hibernate.type.LongType;
import org.hibernate.type.StringType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public class SisTicketConformidadRepositoryImpl extends HibernateRepository<SisTicketConformidad, Long> implements SisTicketConformidadRepository {

    private static final Logger LOGGER = LoggerFactory.getLogger(SisTicketConformidadRepositoryImpl.class);

    @Transactional
    public Long saveSisTicketConformidad(SisTicketConformidad sisTicketConformidad) {
        try {
            Session session = sessionFactory.getCurrentSession();
            return (Long) session.save(sisTicketConformidad);
        } catch (Exception e) {
            LOGGER.error("[saveSisTicketConformidad] - Ocurrio un error: ", e);
            return null;
        }
    }

    @Transactional(readOnly = true)
    public List<ConformidadTicket> getTicketConformidad(Integer cantidad) {
        try {
            final String sql = "SELECT " +
                    "tc.idTicketConformidad AS idTicketConformidad, " +
                    "tc.nombreArchivo AS nombreArchivo, " +
                    "tc.ticket AS ticket, " +
                    "tc.fechaRegistroTicket AS fechaRegistroTicket, " +
                    "tc.estado AS estado " +
                    "FROM sis_ticket_conformidad tc where tc.estado=" + Boolean.TRUE;
            Session session = sessionFactory.getCurrentSession();

            List<ConformidadTicket> conformidadTicketList = session.createSQLQuery(sql)
                    .addScalar("idTicketConformidad", LongType.INSTANCE)
                    .addScalar("nombreArchivo", StringType.INSTANCE)
                    .addScalar("ticket", StringType.INSTANCE)
                    .addScalar("fechaRegistroTicket", DateType.INSTANCE)
                    .addScalar("estado", BooleanType.INSTANCE)
                    .setMaxResults(cantidad)
                    .setResultTransformer(Transformers.aliasToBean(ConformidadTicket.class)).list();
            return conformidadTicketList;
        } catch (Exception e) {
            LOGGER.error("[getTicketConformidad] - Ocurrio un error: ", e);
            return null;
        }
    }

    @Transactional
    public void updateEstadoDeshabilitado(Long idTicketConformidad) {
        try {
            String hql = "UPDATE sis_ticket_conformidad SET estado=:deshabilitado WHERE idTicketConformidad=:idTicketConformidad";
            Session session = sessionFactory.getCurrentSession();
            Query query = session.createSQLQuery(hql);
            query.setParameter("idTicketConformidad", idTicketConformidad);
            query.setParameter("deshabilitado", Boolean.FALSE);
            query.executeUpdate();
        } catch (Exception e) {
            LOGGER.error("[updateEstadoDeshabilitado] - Ocurrio un error: ", e);
        }
    }


}
