package com.net.tci.conformidad.task;

public interface Task {

    void executeTask();

}
