package com.net.tci.conformidad.entity;


import javax.persistence.*;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "sis_correlativo_conformidad")
public class SisCorrelativoConformidad implements java.io.Serializable {

    private static final long serialVersionUID = -3490812332621791334L;

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "idEmpresa", unique = true, nullable = false)
    private Integer idEmpresa;

    @Column(name = "correlativoPnd")
    private Integer correlativoPnd;

    @Column(name = "correlativoDis")
    private Integer correlativoDis;

    @Column(name = "correlativoSub")
    private Integer correlativoSub;

    public Integer getCorrelativoPnd() {
        return correlativoPnd;
    }

    public void setCorrelativoPnd(Integer correlativoPnd) {
        this.correlativoPnd = correlativoPnd;
    }

    public Integer getCorrelativoDis() {
        return correlativoDis;
    }

    public void setCorrelativoDis(Integer correlativoDis) {
        this.correlativoDis = correlativoDis;
    }

    public Integer getCorrelativoSub() {
        return correlativoSub;
    }

    public void setCorrelativoSub(Integer correlativoSub) {
        this.correlativoSub = correlativoSub;
    }
}
