package com.net.tci.conformidad.entity;

import javax.persistence.*;
import java.util.Date;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "sis_ticket_conformidad")
public class SisTicketConformidad implements java.io.Serializable {

    private static final long serialVersionUID = -3230812332634791334L;

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "idTicketConformidad", unique = true, nullable = false)
    private Long idTicketConformidad;

    @Column(name = "nombreArchivo")
    private String nombreArchivo;

    @Column(name = "ticket")
    private String ticket;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "fechaRegistroTicket")
    private Date fechaRegistroTicket;

    @Column(name = "estado")
    private Boolean estado;

    public Boolean getEstado() {
        return estado;
    }

    public void setEstado(Boolean estado) {
        this.estado = estado;
    }

    public Date getFechaRegistroTicket() {
        return fechaRegistroTicket;
    }

    public void setFechaRegistroTicket(Date fechaRegistroTicket) {
        this.fechaRegistroTicket = fechaRegistroTicket;
    }

    public Long getIdTicketConformidad() {
        return idTicketConformidad;
    }

    public void setIdTicketConformidad(Long idTicketConformidad) {
        this.idTicketConformidad = idTicketConformidad;
    }

    public String getNombreArchivo() {
        return nombreArchivo;
    }

    public void setNombreArchivo(String nombreArchivo) {
        this.nombreArchivo = nombreArchivo;
    }

    public String getTicket() {
        return ticket;
    }

    public void setTicket(String ticket) {
        this.ticket = ticket;
    }
}
