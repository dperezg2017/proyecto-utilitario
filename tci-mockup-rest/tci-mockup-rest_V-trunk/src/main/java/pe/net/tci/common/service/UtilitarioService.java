package pe.net.tci.common.service;

import java.io.File;
import java.security.MessageDigest;

public interface UtilitarioService {

    String getFileChecksum(MessageDigest digest, File file);
}
