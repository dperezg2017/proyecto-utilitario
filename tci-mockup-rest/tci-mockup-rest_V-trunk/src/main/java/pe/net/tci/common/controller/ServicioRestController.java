package pe.net.tci.common.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import pe.net.tci.common.domain.DetalleTicketResponse;
import pe.net.tci.common.domain.Error;
import pe.net.tci.common.domain.ErrorConformidadResponse;
import pe.net.tci.common.domain.TicketResponse;
import pe.net.tci.common.util.Constantes;
import pe.net.tci.common.util.Utilitario;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component
@Controller
@RequestMapping("/rest")
public class ServicioRestController {

    private static final Logger LOGGER = LoggerFactory.getLogger(ServicioRestController.class);

    @RequestMapping(value = "/test", method = RequestMethod.GET)
    public
    @ResponseBody
    String getString() {
        return "test";
    }

    @RequestMapping(value = "/exponerArchivo", method = RequestMethod.POST)
    public
    @ResponseBody
    ResponseEntity<String> getConvertTXTtoSHA(
            @RequestParam(value = "valHash") String valHash,
            @RequestParam(value = "archivo") MultipartFile archivo) {

        String response = valHash + " - " + archivo.getName();
        LOGGER.info("response: {}", response);
        return new ResponseEntity<String>(valHash + " - " + archivo.getName(), HttpStatus.OK);
    }

    @RequestMapping(value = "/getErrorConformidadEnviadoAlterno", method = RequestMethod.POST)
    public
    @ResponseBody
    ResponseEntity<?> getErrorConformidadEnviadoAlterno(
            @RequestParam(value = "archivo") MultipartFile archivo,
            @RequestParam(value = "valHash") String valHash) {

        try {
            ErrorConformidadResponse errorConformidadResponse = new ErrorConformidadResponse();
            errorConformidadResponse.setCod("422");
            errorConformidadResponse.setMsg("Unprocessable Entity - Se presentaron errores de validacion que impidieron completar el Request");
            errorConformidadResponse.setExc(null);
            List<Error> errors = new ArrayList<Error>();
            Error error = new Error();
            error.setCodError("210");
            error.setDesError("El archivo " + archivo.getName() + ".txt fue previamente enviado " + valHash);
            errors.add(error);
            errorConformidadResponse.setErrors(errors);

            return new ResponseEntity<ErrorConformidadResponse>(errorConformidadResponse, HttpStatus.UNPROCESSABLE_ENTITY);

        } catch (Exception e) {
            LOGGER.error("Ocurrio un error: ", e);
            return new ResponseEntity<String>("errorConformidadResponse", HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }

    @RequestMapping(value = "/getErrorConformidadEnviado", method = RequestMethod.POST)
    public
    @ResponseBody
    ResponseEntity<?> getErrorConformidadEnviado(@RequestParam MultipartFile archivo, @RequestParam String valHash) {

        try {
            ErrorConformidadResponse errorConformidadResponse = new ErrorConformidadResponse();
            errorConformidadResponse.setCod("422");
            errorConformidadResponse.setMsg("Unprocessable Entity - Se presentaron errores de validacion que impidieron completar el Request");
            errorConformidadResponse.setExc(null);
            List<Error> errors = new ArrayList<Error>();
            Error error = new Error();
            error.setCodError("210");
            error.setDesError("El archivo " + archivo.getName() + ".txt fue previamente enviado");
            errors.add(error);
            errorConformidadResponse.setErrors(errors);

            return new ResponseEntity<ErrorConformidadResponse>(errorConformidadResponse, HttpStatus.UNPROCESSABLE_ENTITY);

        } catch (Exception e) {
            LOGGER.error("Ocurrio un error: ", e);
            return new ResponseEntity<String>("errorConformidadResponse", HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }

    @RequestMapping(value = "/getConformidadTicket", method = RequestMethod.POST)
    public
    @ResponseBody
    ResponseEntity<?> getConformidadTicket(@RequestParam MultipartFile archivo, @RequestParam String valHash) {

        try {
            TicketResponse ticketResponse = new TicketResponse();
            ticketResponse.setNumTicket(Utilitario.generarNumero(10000000, 99999999));

            return new ResponseEntity<TicketResponse>(ticketResponse, HttpStatus.OK);

        } catch (Exception e) {
            LOGGER.error("Ocurrio un error: ", e);
            return new ResponseEntity<String>("ERROR-getConformidadTicket: ", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "/getTicketConformidadLista03", method = RequestMethod.GET)
    public
    @ResponseBody
    ResponseEntity<?> getTicketConformidadLista03(@RequestParam String numTicket) {
        try {
            String base64 = Constantes.BASE64_LISTA_03;
            DetalleTicketResponse detalleTicketResponse = Utilitario.mapearDetalleTicketResponse("03",base64);
            return new ResponseEntity<DetalleTicketResponse>(detalleTicketResponse, HttpStatus.OK);

        } catch (Exception e) {
            LOGGER.error("[getTicketConformidad] - Ocurrio un error: ", e);
            return new ResponseEntity<String>("[getTicketConformidad] - Ocurrio un error: ", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "/getTicketConformidadFila03", method = RequestMethod.GET)
    public
    @ResponseBody
    ResponseEntity<?> getTicketConformidadFila03(@RequestParam String numTicket) {

        try {
            String base64 = Constantes.BASE64_FILA_03;
            DetalleTicketResponse detalleTicketResponse = Utilitario.mapearDetalleTicketResponse("03",base64);
            return new ResponseEntity<DetalleTicketResponse>(detalleTicketResponse, HttpStatus.OK);

        } catch (Exception e) {
            LOGGER.error("[getTicketConformidad] - Ocurrio un error: ", e);
            return new ResponseEntity<String>("[getTicketConformidad] - Ocurrio un error: ", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "/getTicketConformidadFila04", method = RequestMethod.GET)
    public
    @ResponseBody
    ResponseEntity<?> getTicketConformidadFila04(@RequestParam String numTicket) {
        try {
            String base64 = "";
            DetalleTicketResponse detalleTicketResponse = Utilitario.mapearDetalleTicketResponse("04",base64);
            return new ResponseEntity<DetalleTicketResponse>(detalleTicketResponse, HttpStatus.OK);

        } catch (Exception e) {
            LOGGER.error("[getTicketConformidad] - Ocurrio un error: ", e);
            return new ResponseEntity<String>("[getTicketConformidad] - Ocurrio un error: ", HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }


    @RequestMapping(value = "/getTicketConformidadFila02", method = RequestMethod.GET)
    public
    @ResponseBody
    ResponseEntity<?> getTicketConformidadFila02(@RequestParam String numTicket){
        try{
            String base64 = "";
            DetalleTicketResponse detalleTicketResponse = Utilitario.mapearDetalleTicketResponse("02",base64);
            return new ResponseEntity<DetalleTicketResponse>(detalleTicketResponse, HttpStatus.OK);

        }catch (Exception e){
            LOGGER.error("[getTicketConformidad] - Ocurrio un error: ",e);
            return new ResponseEntity<String>("[getTicketConformidad] - Ocurrio un error: ", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}