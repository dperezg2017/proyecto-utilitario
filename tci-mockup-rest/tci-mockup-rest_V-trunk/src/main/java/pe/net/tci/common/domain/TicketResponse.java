package pe.net.tci.common.domain;

public class TicketResponse {

    String numTicket;

    public String getNumTicket() {
        return numTicket;
    }

    public void setNumTicket(String numTicket) {
        this.numTicket = numTicket;
    }
}
