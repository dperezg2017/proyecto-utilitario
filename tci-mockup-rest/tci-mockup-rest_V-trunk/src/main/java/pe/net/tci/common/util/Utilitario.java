package pe.net.tci.common.util;

import pe.net.tci.common.domain.DetalleTicketResponse;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Utilitario {

    public static String generarNumero(Integer min , Integer max){
        Integer aleatorio = (int)Math.floor(Math.random() * (max - min)) + min;
        return  aleatorio.toString();
    }


    public static DetalleTicketResponse mapearDetalleTicketResponse(String codEstadoProceso, String base64){

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMdd");
        SimpleDateFormat simpleDateFormat2 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
        String date = simpleDateFormat.format(new Date());
        String date2 = simpleDateFormat2.format(new Date());

        DetalleTicketResponse detalleTicketResponse = new DetalleTicketResponse();
        detalleTicketResponse.setCodEstadoProceso(codEstadoProceso);
        detalleTicketResponse.setDesArchivo("20112811096-PND-" + date + "-01.txt");
        detalleTicketResponse.setValHash("758be36bc96d7a6a1fa39c6f21b7cb053e51a2f56287995dc2e5a1883dc82516");
        detalleTicketResponse.setFecRegis(date2);
        detalleTicketResponse.setCntRegistrosCpe(3);
        detalleTicketResponse.setCntRegistrosCorrectos(0);
        detalleTicketResponse.setCntRegistrosErrores(3);
        if(codEstadoProceso.equalsIgnoreCase("04")){
            detalleTicketResponse.setCntRegistrosCorrectos(3);
            detalleTicketResponse.setCntRegistrosErrores(0);
        }
        detalleTicketResponse.setArcErrores(base64);

        return detalleTicketResponse;
    }

}
